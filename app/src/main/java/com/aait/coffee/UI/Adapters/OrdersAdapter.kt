package com.aait.coffee.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import com.aait.coffee.Base.ParentRecyclerAdapter
import com.aait.coffee.Base.ParentRecyclerViewHolder
import com.aait.coffee.Models.BaristaProductModel
import com.aait.coffee.Models.OrdersModel
import com.aait.coffee.R
import com.bumptech.glide.Glide

class OrdersAdapter (context: Context, data: MutableList<OrdersModel>, layoutId: Int) :
    ParentRecyclerAdapter<OrdersModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.order_date!!.setText(questionModel.date)
        viewHolder.order_num.text = "#"+questionModel.id.toString()
        if(questionModel.payment.equals("cash")){
            viewHolder.payment_method.text = mcontext.getString(R.string.pay_cash)
        }else{
            viewHolder.payment_method.text = mcontext.getString(R.string.pay_online)
        }



        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var order_date=itemView.findViewById<TextView>(R.id.order_date)
        internal var order_num = itemView.findViewById<TextView>(R.id.order_num)
        internal var payment_method = itemView.findViewById<TextView>(R.id.payment_method)






    }
}