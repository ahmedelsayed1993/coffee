package com.aait.coffee.UI.Adapters

import android.content.Context
import android.content.Intent
import android.view.MotionEvent
import android.view.ScaleGestureDetector

import android.view.View
import android.widget.ImageView

import com.aait.coffee.R
import com.aait.coffee.UI.Activities.ImageActivity
import com.aait.coffee.UI.Activities.ImagesActivity
import com.github.islamkhsh.CardSliderAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_image_slider.view.*
import kotlin.math.max
import kotlin.math.min

class SliderAdapter (context:Context,list : ArrayList<String>) : CardSliderAdapter<String>(list) {


    var list = list
    var context=context
    private lateinit var scaleGestureDetector: ScaleGestureDetector
    private var scaleFactor = 1.0f
    lateinit var image:ImageView
    override fun bindView(position: Int, itemContentView: View, item: String?) {
            image = itemContentView.image
            Picasso.with(context).load(item).resize(300,200).into(image)
            itemContentView.setOnClickListener {
                val intent  = Intent(context, ImagesActivity::class.java)
                intent.putExtra("link",list)
                context.startActivity(intent)
            }


    }


    override fun getItemContentLayout(position: Int) : Int { return R.layout.card_image_slider }

}