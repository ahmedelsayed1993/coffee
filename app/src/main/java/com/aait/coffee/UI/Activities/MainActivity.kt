package com.aait.coffee.UI.Activities

import android.graphics.Color
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.R
import com.aait.coffee.UI.Fragments.*

class MainActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_main
    lateinit var home:LinearLayout
    lateinit var home_image:ImageView
    lateinit var home_text:TextView
    lateinit var categories:LinearLayout
    lateinit var categories_image:ImageView
    lateinit var categories_text:TextView
    lateinit var brands:LinearLayout
    lateinit var brands_image:ImageView
    lateinit var brands_text:TextView
    lateinit var my_cart:LinearLayout
    lateinit var cart_image:ImageView
    lateinit var cart_text:TextView
    lateinit var barista:LinearLayout
    lateinit var barista_image:ImageView
    lateinit var barista_text:TextView
    lateinit var more:LinearLayout
    lateinit var more_image:ImageView
    lateinit var more_text:TextView
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    var type = ""

    private var fragmentManager: FragmentManager? = null
    var selected = 1
    private var transaction: FragmentTransaction? = null
    internal lateinit var homeFragment: HomeFragment
    internal lateinit var categoriesFragment: CategoriesFragment
    internal lateinit var brandsFragment: BrandsFragment
    internal lateinit var myCartFragment: MyCartFragment
    internal lateinit var baristaFragment: BaristaFragment
    internal lateinit var moreFragment: MoreFragment
    override fun initializeComponents() {
        type = intent.getStringExtra("type")
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        home_text = findViewById(R.id.home_text)
        categories = findViewById(R.id.categories)
        categories_image = findViewById(R.id.categories_image)
        categories_text = findViewById(R.id.categories_text)
        brands = findViewById(R.id.brands)
        brands_image = findViewById(R.id.brands_image)
        brands_text = findViewById(R.id.brands_text)
        my_cart = findViewById(R.id.my_cart)
        cart_image = findViewById(R.id.cart_image)
        cart_text = findViewById(R.id.cart_text)
        barista = findViewById(R.id.barista)
        barista_image = findViewById(R.id.barista_image)
        barista_text = findViewById(R.id.barista_text)
        more = findViewById(R.id.more)
        more_image = findViewById(R.id.more_image)
        more_text = findViewById(R.id.more_text)
        homeFragment = HomeFragment.newInstance()
        categoriesFragment = CategoriesFragment.newInstance()
        brandsFragment = BrandsFragment.newInstance()
        myCartFragment = MyCartFragment.newInstance()
        baristaFragment = BaristaFragment.newInstance()
        moreFragment = MoreFragment.newInstance()
        fragmentManager = getSupportFragmentManager()
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.add(R.id.home_fragment_container, homeFragment)
        transaction!!.add(R.id.home_fragment_container, categoriesFragment)
        transaction!!.add(R.id.home_fragment_container,brandsFragment)
        transaction!!.add(R.id.home_fragment_container,myCartFragment)
        transaction!!.add(R.id.home_fragment_container,baristaFragment)
        transaction!!.add(R.id.home_fragment_container,moreFragment)
        transaction!!.commit()
        if (type.equals("normal")) {
            selected = 1
            home_image.setImageResource(R.mipmap.home)
            home_text.textColor = Color.parseColor("#F29561")
            categories_text.textColor = Color.parseColor("#ffffff")
            brands_text.textColor = Color.parseColor("#ffffff")
            cart_text.textColor = Color.parseColor("#ffffff")
            barista_text.textColor = Color.parseColor("#ffffff")
            more_text.textColor = Color.parseColor("#ffffff")
            categories_image.setImageResource(R.mipmap.fde)
            brands_image.setImageResource(R.mipmap.cof)
            cart_image.setImageResource(R.mipmap.shopping)
            barista_image.setImageResource(R.mipmap.coffe)
            more_image.setImageResource(R.mipmap.moro)
            transaction = fragmentManager!!.beginTransaction()
            //        transaction.hide(mMoreFragment);
            //        transaction.hide(mOrdersFragment);
            //        transaction.hide(mFavouriteFragment);
            transaction!!.replace(R.id.home_fragment_container, homeFragment)
            transaction!!.commit()
        }else if (type.equals("cart")){
            selected = 1

            home_text.textColor = Color.parseColor("#ffffff")
            categories_text.textColor = Color.parseColor("#ffffff")
            brands_text.textColor = Color.parseColor("#ffffff")
            cart_text.textColor = Color.parseColor("#F29561")
            barista_text.textColor = Color.parseColor("#ffffff")
            more_text.textColor = Color.parseColor("#ffffff")
            home_image.setImageResource(R.mipmap.hom)
            categories_image.setImageResource(R.mipmap.fde)
            brands_image.setImageResource(R.mipmap.cof)
            cart_image.setImageResource(R.mipmap.shoppin)
            barista_image.setImageResource(R.mipmap.coffe)
            more_image.setImageResource(R.mipmap.moro)
            transaction = fragmentManager!!.beginTransaction()
            //        transaction.hide(mMoreFragment);
            //        transaction.hide(mOrdersFragment);
            //        transaction.hide(mFavouriteFragment);
            transaction!!.replace(R.id.home_fragment_container, myCartFragment)
            transaction!!.commit()
        }
        home.setOnClickListener { showhome() }
        categories.setOnClickListener { showCats() }
        brands.setOnClickListener { showBrands() }
        my_cart.setOnClickListener { showMyCart() }
        barista.setOnClickListener { showBarista() }
        more.setOnClickListener { showMore() }



    }

    fun showhome(){
        selected = 1
        home_image.setImageResource(R.mipmap.home)
        home_text.textColor = Color.parseColor("#F29561")
        categories_text.textColor = Color.parseColor("#ffffff")
        brands_text.textColor = Color.parseColor("#ffffff")
        cart_text.textColor = Color.parseColor("#ffffff")
        barista_text.textColor = Color.parseColor("#ffffff")
        more_text.textColor = Color.parseColor("#ffffff")
        categories_image.setImageResource(R.mipmap.fde)
        brands_image.setImageResource(R.mipmap.cof)
        cart_image.setImageResource(R.mipmap.shopping)
        barista_image.setImageResource(R.mipmap.coffe)
        more_image.setImageResource(R.mipmap.moro)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, homeFragment)
        transaction!!.commit()
    }

    fun showCats(){
        selected = 1

        home_text.textColor = Color.parseColor("#ffffff")
        categories_text.textColor = Color.parseColor("#F29561")
        brands_text.textColor = Color.parseColor("#ffffff")
        cart_text.textColor = Color.parseColor("#ffffff")
        barista_text.textColor = Color.parseColor("#ffffff")
        more_text.textColor = Color.parseColor("#ffffff")
        home_image.setImageResource(R.mipmap.hom)
        categories_image.setImageResource(R.mipmap.ed)
        brands_image.setImageResource(R.mipmap.cof)
        cart_image.setImageResource(R.mipmap.shopping)
        barista_image.setImageResource(R.mipmap.coffe)
        more_image.setImageResource(R.mipmap.moro)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, categoriesFragment)
        transaction!!.commit()
    }

    fun showBrands(){
        selected = 1

        home_text.textColor = Color.parseColor("#ffffff")
        categories_text.textColor = Color.parseColor("#ffffff")
        brands_text.textColor = Color.parseColor("#F29561")
        cart_text.textColor = Color.parseColor("#ffffff")
        barista_text.textColor = Color.parseColor("#ffffff")
        more_text.textColor = Color.parseColor("#ffffff")
        home_image.setImageResource(R.mipmap.hom)
        categories_image.setImageResource(R.mipmap.fde)
        brands_image.setImageResource(R.mipmap.coff)
        cart_image.setImageResource(R.mipmap.shopping)
        barista_image.setImageResource(R.mipmap.coffe)
        more_image.setImageResource(R.mipmap.moro)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, brandsFragment)
        transaction!!.commit()
    }
    fun showMyCart(){
        selected = 1

        home_text.textColor = Color.parseColor("#ffffff")
        categories_text.textColor = Color.parseColor("#ffffff")
        brands_text.textColor = Color.parseColor("#ffffff")
        cart_text.textColor = Color.parseColor("#F29561")
        barista_text.textColor = Color.parseColor("#ffffff")
        more_text.textColor = Color.parseColor("#ffffff")
        home_image.setImageResource(R.mipmap.hom)
        categories_image.setImageResource(R.mipmap.fde)
        brands_image.setImageResource(R.mipmap.cof)
        cart_image.setImageResource(R.mipmap.shoppin)
        barista_image.setImageResource(R.mipmap.coffe)
        more_image.setImageResource(R.mipmap.moro)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, myCartFragment)
        transaction!!.commit()
    }
    fun showBarista(){
        selected = 1

        home_text.textColor = Color.parseColor("#ffffff")
        categories_text.textColor = Color.parseColor("#ffffff")
        brands_text.textColor = Color.parseColor("#ffffff")
        cart_text.textColor = Color.parseColor("#ffffff")
        barista_text.textColor = Color.parseColor("#F29561")
        more_text.textColor = Color.parseColor("#ffffff")
        home_image.setImageResource(R.mipmap.hom)
        categories_image.setImageResource(R.mipmap.fde)
        brands_image.setImageResource(R.mipmap.cof)
        cart_image.setImageResource(R.mipmap.shopping)
        barista_image.setImageResource(R.mipmap.coffee)
        more_image.setImageResource(R.mipmap.moro)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, baristaFragment)
        transaction!!.commit()
    }
    fun showMore(){
        selected = 1

        home_text.textColor = Color.parseColor("#ffffff")
        categories_text.textColor = Color.parseColor("#ffffff")
        brands_text.textColor = Color.parseColor("#ffffff")
        cart_text.textColor = Color.parseColor("#ffffff")
        barista_text.textColor = Color.parseColor("#ffffff")
        more_text.textColor = Color.parseColor("#F29561")
        home_image.setImageResource(R.mipmap.hom)
        categories_image.setImageResource(R.mipmap.fde)
        brands_image.setImageResource(R.mipmap.cof)
        cart_image.setImageResource(R.mipmap.shopping)
        barista_image.setImageResource(R.mipmap.coffe)
        more_image.setImageResource(R.mipmap.mo)
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, moreFragment)
        transaction!!.commit()
    }

}