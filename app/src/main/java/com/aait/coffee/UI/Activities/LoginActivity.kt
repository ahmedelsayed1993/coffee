package com.aait.coffee.UI.Activities

import android.content.Intent
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.Models.UserResponse
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.Utils.CommonUtil
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_login
     lateinit var register: Button
    lateinit var phone:EditText
    lateinit var password:EditText
    lateinit var login:Button
    lateinit var forgot_pass:TextView
    var deviceID = ""
    lateinit var back:ImageView
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        register = findViewById(R.id.register)
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        login = findViewById(R.id.login)
        forgot_pass = findViewById(R.id.forgot_pass)
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
        finish()}
        register.setOnClickListener { startActivity(Intent(this,RegisterActivity::class.java)) }
        login.setOnClickListener {
            if(CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                  CommonUtil.checkEditError(password,getString(R.string.password))){
                return@setOnClickListener
            }else{
                Login()
            }
        }

        forgot_pass.setOnClickListener { startActivity(Intent(this,ForgotPassActivity::class.java))
        finish()}

    }

    fun Login(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Login(phone.text.toString()
            ,password.text.toString(),deviceID,"android",lang.appLanguage)?.enqueue(object:
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
                Log.e("error", Gson().toJson(t))
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        user.loginStatus = true
                        user.userData = response.body()?.data!!
                        val intent = Intent(this@LoginActivity,MainActivity::class.java)
                        intent.putExtra("type","normal")
                        startActivity(intent)
                        finish()
                    }else if (response.body()?.value.equals("2")){
                        val intent = Intent(this@LoginActivity,ActivationCodeActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}