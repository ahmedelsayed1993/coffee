package com.aait.coffee.UI.Fragments

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.coffee.Base.BaseFragment
import com.aait.coffee.Cart.AllCartViewModel
import com.aait.coffee.Cart.CartDataBase
import com.aait.coffee.Cart.CartModelOffline
import com.aait.coffee.Listeners.OnItemClickListener
import com.aait.coffee.Models.*
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.QuickClient
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.UI.Activities.LoginActivity
import com.aait.coffee.UI.Activities.OrderLocationActivity
import com.aait.coffee.UI.Activities.ProductDetailsActivity
import com.aait.coffee.UI.Adapters.CartAdapter
import com.aait.coffee.UI.Adapters.SimilarAdapter
import com.aait.coffee.Utils.CommonUtil
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_follow_order.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class MyCartFragment:BaseFragment(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if(view.id == R.id.plus) {
            val dialog = Dialog(mContext!!)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            dialog?.setCancelable(true)
            dialog?.setContentView(R.layout.dialog_delete)

            val confirm = dialog?.findViewById<Button>(R.id.confirm)
            val cancel = dialog?.findViewById<Button>(R.id.cancel)
            confirm.setOnClickListener {
                allCartViewModel!!.deleteItem(cartAdapter.data.get(position))
                allCartViewModel!!.allCart.observe(this, object : Observer<List<CartModelOffline>> {
                    override fun onChanged(@Nullable cartModels: List<CartModelOffline>) {
                        cartModelOfflines = cartModels
                        if (cartModels.size == 0) {
                            lay.visibility = View.GONE
                            lay_no.visibility = View.VISIBLE

                        } else {

                            lay.visibility = View.VISIBLE
                            lay_no.visibility = View.GONE
                            cartAdapter.updateAll(cartModels)
                            Log.e("carts", Gson().toJson(cartModels))
                            basket.clear()
                            price = 0F
                            count = 0
                            product = cartModels.get(0).product_id
                            for (i in 0..cartModels?.size - 1) {
                                basket.add(
                                    BasketModel(
                                        cartModels.get(i).product_id.toString(),
                                        cartModels.get(i).count,
                                        ((cartModels.get(i).price!!.toFloat()) / (cartModels.get(i).count!!)).toString()
                                    )
                                )
                                price = price + cartModels.get(i).price!!.toFloat()
                                count = count + cartModels.get(i).count!!.toInt()

                            }
                            delivery_service.text = (delivery).toString() //+ getString(R.string.rs)
                            //  total_before_tax.text = price.toString() + getString(R.string.rs)
                            tax_price.text =
                                (((((delivery) + price)) * tax) / 100).toString()//+ getString(R.string.rs)
                            total.text =
                                ((((((delivery) + price)) * tax) / 100) + (((delivery) + price))).toString()//+getString(R.string.rs)


                        }


                    }
                })

                dialog?.dismiss()
            }
            cancel.setOnClickListener { dialog?.dismiss() }




            dialog?.show()
        }else{
            val intent = Intent(activity, ProductDetailsActivity::class.java)
            intent.putExtra("id",similarProducts.get(position).id)
            startActivity(intent)
        }

    }

    override val layoutResource: Int
        get() = R.layout.fragment_my_cart
    companion object {
        fun newInstance(): MyCartFragment {
            val args = Bundle()
            val fragment = MyCartFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var carts:RecyclerView
    lateinit var cartOffline: CartModelOffline
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<CartModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<CartModelOffline> = ArrayList()
    internal lateinit var cartAdapter: CartAdapter
    var cartModel = ArrayList<CartModelOffline>()
    lateinit var linearLayoutManager: LinearLayoutManager
    var basket = ArrayList<BasketModel>()
    var delivery = 0F
    var tax = 0
    lateinit var added_tax:TextView
    lateinit var delivery_service:TextView
    lateinit var tax_price:TextView
    lateinit var total:TextView
    var price = 0F
    var count = 0
    var product = 0
    var tax_value = ""
    lateinit var products:RecyclerView
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var similarAdapter: SimilarAdapter
    var similarProducts = ArrayList<BaristaProductModel>()
    lateinit var next:Button
    lateinit var lay:LinearLayout
    lateinit var lay_no:LinearLayout
    override fun initializeComponents(view: View) {
        basket.clear()
        carts = view.findViewById(R.id.carts)
        products = view.findViewById(R.id.products)
        next = view.findViewById(R.id.next)
        lay = view.findViewById(R.id.lay)
        lay_no = view.findViewById(R.id.lay_no)
        added_tax = view.findViewById(R.id.added_tax)
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
        similarAdapter = SimilarAdapter(mContext!!,similarProducts,R.layout.recycler_similar)
        similarAdapter.setOnItemClickListener(this)
        products.layoutManager = linearLayoutManager1
        products.adapter = similarAdapter
        delivery_service = view.findViewById(R.id.delivery_service)
        tax_price = view.findViewById(R.id.tax_price)
        total = view.findViewById(R.id.total)
        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        cartAdapter = CartAdapter(mContext!!,cartModel,R.layout.reccler_cart)
        cartAdapter.setOnItemClickListener(this)
        carts.layoutManager = linearLayoutManager
        carts.adapter = cartAdapter

        cartModels = allCartViewModel!!.allCart
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)

        allCartViewModel!!.allCart.observe(this, object : Observer<List<CartModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<CartModelOffline>) {
                cartModelOfflines = cartModels
                if(cartModels.size==0){
                    lay.visibility = View.GONE
                    lay_no.visibility = View.VISIBLE
                }else {

                    lay.visibility = View.VISIBLE
                    lay_no.visibility = View.GONE
                    cartAdapter.updateAll(cartModels)

                    Log.e("carts", Gson().toJson(cartModels))
                    basket.clear()
                    price = 0F
                    count = 0
                    product = cartModels.get(0).product_id
                    getData()
                    for (i in 0..cartModels?.size - 1) {
                        basket.add(BasketModel(cartModels.get(i).product_id.toString(),cartModels.get(i).count,((cartModels.get(i).price!!.toFloat())/(cartModels.get(i).count!!)).toString()))
                        price = price+cartModels.get(i).price!!.toFloat()
                        count = count+cartModels.get(i).count!!.toInt()

                    }
                    //delivery_service.text = (delivery).toString() //+ getString(R.string.rs)
                    //  total_before_tax.text = price.toString() + getString(R.string.rs)
                    tax_price.text = ((((price))*tax)/100).toString()//+ getString(R.string.rs)
                    total.text = (((((price))*tax)/100)+((price))).toString()//+getString(R.string.rs)
                    Log.e("basket",Gson().toJson(basket))


                }


            }
        })
        next.setOnClickListener {
            if (user.loginStatus!!) {
                Token()
            }else{
                startActivity(Intent(activity, LoginActivity::class.java))
                activity?.finish()
            }
        }
    }
    fun Token(){
        showProgressDialog(getString(R.string.please_wait))
        QuickClient.getClient()?.create(Service::class.java)?.Token("cafestation","cafestation2020")?.enqueue(object:Callback<NewTokenResponse>{
            override fun onFailure(call: Call<NewTokenResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<NewTokenResponse>,
                response: Response<NewTokenResponse>
            ) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if(response.body()?.httpStatusCode==200){
                        val intent = Intent(activity, OrderLocationActivity::class.java)

                        intent.putExtra("tax", tax_price.text)
                        intent.putExtra("price", price)
                        intent.putExtra("tax_value", tax_value)
                        intent.putExtra("total", total.text)
                        intent.putExtra("order", basket)
                        intent.putExtra("token", "Bearer "+response.body()?.resultData?.access_token)
                        startActivity(intent)
                    }else{
                           if(lang.appLanguage.equals("ar")) {
                               CommonUtil.makeToast(mContext!!, response.body()?.messageAr!!)
                           }else{
                               CommonUtil.makeToast(mContext!!, response.body()?.messageEn!!)
                           }

                    }
                }
            }

        })
    }
    fun getData(){
        // showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Coast()?.enqueue(object:
            Callback<CoastResponse> {
            override fun onFailure(call: Call<CoastResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<CoastResponse>, response: Response<CoastResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        //delivery = response.body()?.data?.delivery_price!!.toFloat()
                        added_tax.text = mContext!!.getString(R.string.tax)+response.body()?.data?.tax + "%"
                        tax = response.body()?.data?.tax!!.toInt()
                       // delivery_service.text = (delivery).toString() //+ getString(R.string.rs)
                        tax_value = response.body()?.data?.tax!!
                        //  total_before_tax.text = price.toString() + getString(R.string.rs)
                        tax_price.text = ((((price))*tax)/100).toString()//+ getString(R.string.rs)
                        total.text = (((((price))*tax)/100)+((price))).toString()//+getString(R.string.rs)

                    }
                }
            }

        })

        Client.getClient()?.create(Service::class.java)?.Similar(lang.appLanguage,product)?.enqueue(object:Callback<SimilarProductsResponse>{
            override fun onFailure(call: Call<SimilarProductsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SimilarProductsResponse>,
                response: Response<SimilarProductsResponse>
            ) {
                hideProgressDialog()
                if(response.body()?.value.equals("1")){
                    similarAdapter.updateAll(response.body()?.data!!)
                }else{
                    CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                }
            }

        })

    }
}