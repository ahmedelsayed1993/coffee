package com.aait.coffee.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.coffee.Base.BaseFragment
import com.aait.coffee.Listeners.OnItemClickListener
import com.aait.coffee.Models.CatModel
import com.aait.coffee.Models.HomeResponse
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.UI.Activities.BaristaActivity
import com.aait.coffee.UI.Activities.ProductsActivity
import com.aait.coffee.UI.Adapters.BrandsAdapter
import com.aait.coffee.UI.Adapters.CatsAdapter
import com.aait.coffee.UI.Adapters.SliderAdapter
import com.aait.coffee.Utils.CommonUtil
import com.github.islamkhsh.CardSliderViewPager
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragment:BaseFragment(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.cat){
            if(catsModels.get(position).id==1){
                startActivity(Intent(activity, BaristaActivity::class.java))
            }else{
                val intent = Intent(activity, ProductsActivity::class.java)
                intent.putExtra("id",catsModels.get(position).id)
                intent.putExtra("type","category")
                intent.putExtra("name",catsModels.get(position).name)
                intent.putExtra("image",catsModels.get(position).image)
                startActivity(intent)
            }
        }else{
            val intent = Intent(activity, ProductsActivity::class.java)
            intent.putExtra("id",brandsModels.get(position).id)
            intent.putExtra("type","brand")
            intent.putExtra("name",brandsModels.get(position).name)
            intent.putExtra("image",brandsModels.get(position).image)
            startActivity(intent)
        }

    }

    override val layoutResource: Int
        get() = R.layout.fragment_home
    companion object {
        fun newInstance(): HomeFragment {
            val args = Bundle()
            val fragment = HomeFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var viewpager: CardSliderViewPager
    lateinit var categories:RecyclerView
    lateinit var linearLayoutManager: GridLayoutManager
    lateinit var catsAdapter: CatsAdapter
    var catsModels = ArrayList<CatModel>()
    lateinit var brands:RecyclerView
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var brandsAdapter: BrandsAdapter
    var brandsModels = ArrayList<CatModel>()
    override fun initializeComponents(view: View) {
        viewpager = view.findViewById(R.id.viewPager)
        categories = view.findViewById(R.id.categories)

        catsAdapter = CatsAdapter(mContext!!,catsModels,R.layout.recycler_category)
        catsAdapter.setOnItemClickListener(this)

        brands = view.findViewById(R.id.brands)
        gridLayoutManager = GridLayoutManager(mContext!!,2)
        brandsAdapter = BrandsAdapter(mContext!!,brandsModels,R.layout.recycler_brands)
        brandsAdapter.setOnItemClickListener(this)
        brands.layoutManager = gridLayoutManager
        brands.adapter = brandsAdapter
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))

        Client.getClient()?.create(Service::class.java)?.getHome(lang.appLanguage)?.enqueue(object :
            Callback<HomeResponse> {
            override fun onResponse(call: Call<HomeResponse>, response: Response<HomeResponse>) {

                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if (response.isSuccessful) {

                    if (response.body()?.value.equals("1")) {
                        initSliderAds(response.body()?.sliders!!)
                        linearLayoutManager = GridLayoutManager(mContext!!,response.body()?.categories?.size!!)
                        categories.layoutManager = linearLayoutManager
                        categories.adapter = catsAdapter
                        catsAdapter.updateAll(response.body()?.categories!!)
                        brandsAdapter.updateAll(response.body()?.brands!!)

                    } else {}

                }
            }
            override fun onFailure(call: Call<HomeResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                Log.e("response", Gson().toJson(t))
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
            }
        })

    }

    fun initSliderAds(list:ArrayList<String>){
        if(list.isEmpty()){
            viewpager.visibility=View.GONE

        }
        else{
            viewpager.visibility=View.VISIBLE

            viewpager.adapter= SliderAdapter(mContext!!,list)

        }
    }
}