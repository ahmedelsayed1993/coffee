package com.aait.coffee.UI.Activities

import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.Listeners.OnItemClickListener
import com.aait.coffee.Models.BaristaResponse
import com.aait.coffee.Models.CatModel
import com.aait.coffee.Models.ProductModel
import com.aait.coffee.Models.ProductsResponse
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.UI.Adapters.BaristaAdapter
import com.aait.coffee.UI.Adapters.ProductsAdapter
import com.aait.coffee.Utils.CommonUtil
import com.bumptech.glide.Glide
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import androidx.core.widget.addTextChangedListener as addTextChangedListener1
import kotlin.collections.filter as filter1

class ProductsActivity :Parent_Activity(),OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this,ProductDetailsActivity::class.java)
        intent.putExtra("id",baristaModels.get(position).id)

        startActivity(intent)
    }

    override val layoutResource: Int
        get() = R.layout.activity_products
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var text:EditText
    lateinit var image:ImageView
    lateinit var filter:ImageView
    lateinit var filter_lay:LinearLayout
    lateinit var high:TextView
    lateinit var low:TextView

    var id = 0
    var type = ""
    var name = ""
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    var baristaModels=ArrayList<ProductModel>()
    var baristaModels1=ArrayList<ProductModel>()
    lateinit var baristaAdapter: ProductsAdapter
    lateinit var productModel: ProductModel
    var img = ""
    lateinit var ly:LinearLayout

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        type = intent.getStringExtra("type")
        name = intent.getStringExtra("name")
        img = intent.getStringExtra("image")
        ly = findViewById(R.id.ly)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        text = findViewById(R.id.text)
        image = findViewById(R.id.image)
        filter = findViewById(R.id.filter)
        filter_lay = findViewById(R.id.filter_lay)
        high = findViewById(R.id.high)
        low = findViewById(R.id.low)
        filter_lay.visibility = View.GONE
        filter.setOnClickListener { if (filter_lay.visibility==View.GONE){
        filter_lay.visibility = View.VISIBLE
        }else{
            filter_lay.visibility = View.GONE
        }
        }
        high.setOnClickListener { filter_lay.visibility = View.GONE
            filterData("max")}
        low.setOnClickListener { filter_lay.visibility = View.GONE
            filterData("min")}
        back.setOnClickListener { onBackPressed()
        finish()}
        Glide.with(mContext).load(img).into(image)
        title.text = name
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        baristaAdapter = ProductsAdapter(mContext,baristaModels,R.layout.recycler_products)
        baristaAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = baristaAdapter
        ly.setOnClickListener {
            Log.e("click","dddd")
            val intent = Intent(this,ImageActivity::class.java)
            intent.putExtra("link",img)
            startActivity(intent)
        }

      //  baristaModels1 =  baristaModels.filterNotTo()<ProductModel> { baristaModels -> baristaModels.name == "tt" } as ArrayList<ProductModel>
       text.addTextChangedListener(object : TextWatcher{
           override fun afterTextChanged(p0: Editable?) {

           }

           override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

           }

           override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
               baristaModels1.clear()
               for (i in 0.. baristaModels.size-1){
                   if (baristaModels.get(i).name!!.contains(p0.toString())){
                       baristaModels1.add(baristaModels.get(i))
                   }
               }
               //hideKeyboard()
               if (baristaModels1.isEmpty()!!) {
                   layNoItem!!.visibility = View.VISIBLE
                   layNoInternet!!.visibility = View.GONE
                   tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)


               } else {
                   baristaAdapter.updateAll(baristaModels1)
               }
               Log.e("ttt",Gson().toJson(baristaModels1))
               if (p0.toString().equals("")){
                   getData()
               }
           }

       })



        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()


    }
    fun AppCompatActivity.hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        // else {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        // }
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Products(lang.appLanguage,id,type)?.enqueue(object:
            Callback<ProductsResponse> {
            override fun onFailure(call: Call<ProductsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()

                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<ProductsResponse>,
                response: Response<ProductsResponse>
            ) {

                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        Log.e("myJobs", Gson().toJson(response.body()!!.data))
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)


                        } else {
//
                            baristaAdapter.updateAll(response.body()!!.data!!)
                           // adapter1 = ArrayAdapter(mContext,R.layout.recycler_products,response.body()?.data!!)
                        }
//
                    }else {
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }

                }
            }

        })
    }
    fun filterData(price:String){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Filter(lang.appLanguage,id,type,price)?.enqueue(object:
            Callback<ProductsResponse> {
            override fun onFailure(call: Call<ProductsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<ProductsResponse>,
                response: Response<ProductsResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        Log.e("myJobs", Gson().toJson(response.body()!!.data))
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)


                        } else {
//
                            baristaAdapter.updateAll(response.body()!!.data!!)
                            // adapter1 = ArrayAdapter(mContext,R.layout.recycler_products,response.body()?.data!!)
                        }
//
                    }else {
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }

                }
            }

        })
    }

}


