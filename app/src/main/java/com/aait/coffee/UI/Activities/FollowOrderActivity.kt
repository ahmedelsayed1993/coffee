package com.aait.coffee.UI.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.view.View
import android.view.Window
import android.widget.*
import androidx.annotation.RequiresApi
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.Models.TermsResponse
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FollowOrderActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_follow_order
    lateinit var title:TextView
    lateinit var image1:ImageView
    lateinit var image2:ImageView
    lateinit var image3:ImageView
    lateinit var image4:ImageView
    lateinit var next:Button
    lateinit var finish:Button
    var id = 0

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        title = findViewById(R.id.title)
        image1 = findViewById(R.id.image1)
        image2 = findViewById(R.id.image2)
        image3 = findViewById(R.id.image3)
        image4 = findViewById(R.id.image4)
        next = findViewById(R.id.next)
        finish = findViewById(R.id.finish)
        title.text = getString(R.string.follow_order)
        getData()
        finish.setOnClickListener {
            Complete()
        }

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Track("Bearer "+user.userData.token,lang.appLanguage,id)
            ?.enqueue(object: Callback<TermsResponse>{
                override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun onResponse(
                    call: Call<TermsResponse>,
                    response: Response<TermsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            if (response.body()?.data!!.equals("current")){
                                image1.setImageResource(R.mipmap.righ)
                                image2.setImageDrawable(mContext.getDrawable(R.drawable.white_black_circle))
                                image3.setImageDrawable(mContext.getDrawable(R.drawable.white_black_circle))
                                image4.setImageDrawable(mContext.getDrawable(R.drawable.white_black_circle))
                                finish.visibility = View.GONE
                            }else if (response.body()?.data!!.equals("accept")){
                                finish.visibility = View.GONE
                                image1.setImageResource(R.mipmap.righ)
                                image2.setImageResource(R.mipmap.righ)
                                image3.setImageDrawable(mContext.getDrawable(R.drawable.white_black_circle))
                                image4.setImageDrawable(mContext.getDrawable(R.drawable.white_black_circle))
                            }else if(response.body()?.data!!.equals("delivery_order")){
                                finish.visibility = View.VISIBLE
                                image1.setImageResource(R.mipmap.righ)
                                image2.setImageResource(R.mipmap.righ)
                                image3.setImageResource(R.mipmap.righ)
                                image4.setImageDrawable(mContext.getDrawable(R.drawable.white_black_circle))

                            }else if (response.body()?.data!!.equals("completed")){
                                finish.visibility = View.GONE
                                image1.setImageResource(R.mipmap.righ)
                                image2.setImageResource(R.mipmap.righ)
                                image3.setImageResource(R.mipmap.righ)
                                image4.setImageResource(R.mipmap.righ)
                            }
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }

    fun Complete(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Finish("Bearer "+user.userData.token,lang.appLanguage,id,"completed",1)
            ?.enqueue(object: Callback<TermsResponse>{
                override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun onResponse(
                    call: Call<TermsResponse>,
                    response: Response<TermsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            if (response.body()?.data!!.equals("current")){
                                image1.setImageResource(R.mipmap.righ)
                                image2.setImageDrawable(mContext.getDrawable(R.drawable.white_black_circle))
                                image3.setImageDrawable(mContext.getDrawable(R.drawable.white_black_circle))
                                image4.setImageDrawable(mContext.getDrawable(R.drawable.white_black_circle))
                                finish.visibility = View.GONE
                            }else if (response.body()?.data!!.equals("accept")){
                                finish.visibility = View.GONE
                                image1.setImageResource(R.mipmap.righ)
                                image2.setImageResource(R.mipmap.righ)
                                image3.setImageDrawable(mContext.getDrawable(R.drawable.white_black_circle))
                                image4.setImageDrawable(mContext.getDrawable(R.drawable.white_black_circle))
                            }else if(response.body()?.data!!.equals("delivery_order")){
                                finish.visibility = View.VISIBLE
                                image1.setImageResource(R.mipmap.righ)
                                image2.setImageResource(R.mipmap.righ)
                                image3.setImageResource(R.mipmap.righ)
                                image4.setImageDrawable(mContext.getDrawable(R.drawable.white_black_circle))

                            }else if (response.body()?.data!!.equals("completed")){
                                finish.visibility = View.GONE
                                image1.setImageResource(R.mipmap.righ)
                                image2.setImageResource(R.mipmap.righ)
                                image3.setImageResource(R.mipmap.righ)
                                image4.setImageResource(R.mipmap.righ)
                            }

                            val dialog =  Dialog(mContext)
                            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                            dialog?.setCancelable(true)
                            dialog?.setContentView(R.layout.dialog_rate)
                            val rating = dialog?.findViewById<RatingBar>(R.id.rating)
                            val comment = dialog?.findViewById<EditText>(R.id.comment)
                            val confirm = dialog?.findViewById<Button>(R.id.confirm)
                            confirm!!.setOnClickListener { if (CommonUtil.checkEditError(comment,getString(R.string.write_comment))){
                            return@setOnClickListener
                            }else{
                                showProgressDialog(getString(R.string.please_wait))
                                Client.getClient()?.create(Service::class.java)?.RateOrder("Bearer "+user.userData.token,lang.appLanguage,id,rating.rating.toInt()
                                ,comment.text.toString())?.enqueue(object:Callback<TermsResponse>{
                                    override fun onFailure(
                                        call: Call<TermsResponse>,
                                        t: Throwable
                                    ) {
                                        CommonUtil.handleException(mContext,t)
                                        t.printStackTrace()
                                        hideProgressDialog()
                                    }

                                    override fun onResponse(
                                        call: Call<TermsResponse>,
                                        response: Response<TermsResponse>
                                    ) {
                                        hideProgressDialog()
                                        if (response.isSuccessful){
                                            if (response.body()?.value.equals("1")){
                                                CommonUtil.makeToast(mContext,response.body()?.data!!)
                                                dialog?.dismiss()
                                                val intent = Intent(this@FollowOrderActivity,MainActivity::class.java)
                                                intent.putExtra("type","normal")
                                                startActivity(intent)
                                                finish()
                                            }else{
                                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                                dialog?.dismiss()
                                                val intent = Intent(this@FollowOrderActivity,MainActivity::class.java)
                                                intent.putExtra("type","normal")
                                                startActivity(intent)
                                                finish()
                                            }
                                        }
                                    }

                                })
                            }
                            }
                            dialog?.show()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}