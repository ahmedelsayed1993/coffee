package com.aait.coffee.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.coffee.Base.BaseFragment
import com.aait.coffee.Listeners.OnItemClickListener
import com.aait.coffee.Models.BaristaResponse
import com.aait.coffee.Models.CatModel
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.UI.Activities.BaristaActivity
import com.aait.coffee.UI.Activities.ProductsActivity
import com.aait.coffee.UI.Adapters.BrandsAdapter
import com.aait.coffee.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CategoriesFragment:BaseFragment(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if(baristaModels.get(position).id==1){
            startActivity(Intent(activity, BaristaActivity::class.java))
        }else{
            val intent = Intent(activity, ProductsActivity::class.java)
            intent.putExtra("id",baristaModels.get(position).id)
            intent.putExtra("type","category")
            intent.putExtra("name",baristaModels.get(position).name)
            intent.putExtra("image",baristaModels.get(position).image)
            startActivity(intent)
        }

    }

    override val layoutResource: Int
        get() = R.layout.fragment_categories

    companion object {
        fun newInstance(): CategoriesFragment {
            val args = Bundle()
            val fragment = CategoriesFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: GridLayoutManager
    var baristaModels=ArrayList<CatModel>()
    lateinit var baristaAdapter: BrandsAdapter
    override fun initializeComponents(view: View) {
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = GridLayoutManager(mContext, 2)
        baristaAdapter = BrandsAdapter(mContext!!,baristaModels,R.layout.recycler_brands)
        baristaAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = baristaAdapter

        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()

    }

    fun getData(){
      //  showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Categories(lang.appLanguage)?.enqueue(object:
            Callback<BaristaResponse> {
            override fun onFailure(call: Call<BaristaResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<BaristaResponse>,
                response: Response<BaristaResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        Log.e("myJobs", Gson().toJson(response.body()!!.data))
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            baristaAdapter.updateAll(response.body()!!.data!!)
                        }
//
                    }else {
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }

                }
            }

        })
    }
}