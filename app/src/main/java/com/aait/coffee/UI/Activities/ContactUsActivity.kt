package com.aait.coffee.UI.Activities

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.Models.SocialResponse
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactUsActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_contact_us
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var name:EditText
    lateinit var phone:EditText
    lateinit var message:EditText
    lateinit var confirm:Button
    lateinit var youtube:ImageView
    lateinit var whats:ImageView
    lateinit var instagram:ImageView
    lateinit var twitter:ImageView
    lateinit var facebook:ImageView
    var you = ""
    var what = ""
    var insta = ""
    var twitt = ""
    var face = ""

    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        message = findViewById(R.id.message)
        confirm = findViewById(R.id.confirm)
        youtube = findViewById(R.id.youtube)
        whats = findViewById(R.id.whats)
        instagram = findViewById(R.id.instagram)
        twitter = findViewById(R.id.twitter)
        facebook = findViewById(R.id.facebook)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.contact_us)
        getData()
        facebook.setOnClickListener { if (!face.equals(""))
        {
            if (face.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(face)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$face"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        } }
        instagram.setOnClickListener {if (!insta.equals(""))
        {
            if (insta.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(insta)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$insta"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        }  }
        youtube.setOnClickListener { if (!you.equals(""))
        {
            if (you.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(you)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$you"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        } }
        twitter.setOnClickListener { if (!twitt.equals(""))
        {
            if (twitt.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(twitt)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$twitt"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        } }
        whats.setOnClickListener {openWhatsAppConversationUsingUri(mContext!!,what.replace("0","+966",false),"")  }

        confirm.setOnClickListener { if (CommonUtil.checkEditError(name,getString(R.string.name))||
                CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(message,getString(R.string.write_message))){
        return@setOnClickListener
        }else{
            Send()
        }
        }

    }
    fun openWhatsAppConversationUsingUri(
        context: Context,
        numberWithCountryCode: String,
        message: String
    ) {

        val uri =
            Uri.parse("https://api.whatsapp.com/send?phone=$numberWithCountryCode&text=$message")

        val sendIntent = Intent(Intent.ACTION_VIEW, uri)

        context.startActivity(sendIntent)
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Contact(lang.appLanguage,null,null,null)?.enqueue(object :
            Callback<SocialResponse> {
            override fun onFailure(call: Call<SocialResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SocialResponse>,
                response: Response<SocialResponse>
            ) {
                hideProgressDialog()
                if (response.body()?.value.equals("1")){
                    you = response.body()?.data?.get(0)?.link!!
                    what = response.body()?.data?.get(1)?.link!!
                    insta = response.body()?.data?.get(2)?.link!!
                    twitt = response.body()?.data?.get(3)?.link!!
                    face = response.body()?.data?.get(4)?.link!!
                }else{
                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                }
            }
        })
    }
    fun Send(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Contact(lang.appLanguage,name.text.toString(),phone.text.toString(),message.text.toString())?.enqueue(object :
            Callback<SocialResponse> {
            override fun onFailure(call: Call<SocialResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SocialResponse>,
                response: Response<SocialResponse>
            ) {
                hideProgressDialog()
                if (response.body()?.value.equals("1")){
                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    name.setText("")
                    phone.setText("")
                    message.setText("")
                    you = response.body()?.data?.get(0)?.link!!
                    what = response.body()?.data?.get(1)?.link!!
                    insta = response.body()?.data?.get(2)?.link!!
                    twitt = response.body()?.data?.get(3)?.link!!
                    face = response.body()?.data?.get(4)?.link!!
                }else{
                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                }
            }
        })
    }
}