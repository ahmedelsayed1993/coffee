package com.aait.coffee.UI.Activities

import android.content.Intent
import android.os.Handler
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.R

class SplashActivity : Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_splash
    var isSplashFinishid = false
    override fun initializeComponents() {
        Handler().postDelayed({
           // logo.startAnimation(logoAnimation2)
            Handler().postDelayed({
                isSplashFinishid=true
                if (user.loginStatus==true){

                    val intent = Intent(this@SplashActivity,MainActivity::class.java)
                    intent.putExtra("type","normal")
                    startActivity(intent)
                    this@SplashActivity.finish()

                }else {

                    var intent = Intent(this@SplashActivity, ChooseLangActivity::class.java)
                    startActivity(intent)
                    finish()
                }

            }, 2100)
        }, 1800)
    }


}
