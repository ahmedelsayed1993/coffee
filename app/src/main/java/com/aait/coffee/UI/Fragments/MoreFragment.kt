package com.aait.coffee.UI.Fragments

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aait.coffee.Base.BaseFragment
import com.aait.coffee.Cart.AllCartViewModel
import com.aait.coffee.Cart.CartDataBase
import com.aait.coffee.Cart.CartModelOffline
import com.aait.coffee.Models.TermsResponse
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.UI.Activities.*
import com.aait.coffee.Utils.CommonUtil
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoreFragment :BaseFragment(){
    override val layoutResource: Int
        get() = R.layout.fragment_more
    companion object {
        fun newInstance(): MoreFragment {
            val args = Bundle()
            val fragment = MoreFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var logout:LinearLayout
    lateinit var about_app:LinearLayout
    lateinit var terms:LinearLayout
    lateinit var follow_us:LinearLayout
    lateinit var whats:LinearLayout
    lateinit var share_app:LinearLayout
    lateinit var call_us:LinearLayout
    lateinit var orders:LinearLayout
    lateinit var fav:LinearLayout
    lateinit var image:CircleImageView
    lateinit var name:TextView
    lateinit var email:TextView
    var phone = ""
    lateinit var icon:ImageView
    lateinit var text:TextView
    lateinit var notification:LinearLayout
    lateinit var profile:LinearLayout
    lateinit var company:LinearLayout
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<CartModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<CartModelOffline> = java.util.ArrayList()
    override fun initializeComponents(view: View) {
        icon = view.findViewById(R.id.icon)
        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        allCartViewModel!!.allCart.observe(this, object : Observer<List<CartModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<CartModelOffline>) {
                cartModelOfflines = cartModels
            }
        })
        logout = view.findViewById(R.id.logout)
        text = view.findViewById(R.id.text)
        if (user.loginStatus!!){
            text.text = mContext!!.getString(R.string.logout)
            icon.setImageResource(R.mipmap.out)
        }else{
            text.text = mContext!!.getString(R.string.login)
            icon.setImageResource(R.mipmap.enter)
        }
        company = view.findViewById(R.id.company)
        company.setOnClickListener {startActivity(Intent(activity, CompaniesActivity::class.java))  }
        profile = view.findViewById(R.id.profile)
        profile.setOnClickListener { if (user.loginStatus!!) {
            startActivity(Intent(activity, ProfileActivity::class.java))
        }else{
            visitor()
        }  }
        notification = view.findViewById(R.id.notification)
        notification.setOnClickListener {  if (user.loginStatus!!) {
            startActivity(Intent(activity, NotificationActivity::class.java))
        }else{
            visitor()
        } }
        logout.setOnClickListener { if (user.loginStatus!!) {
            logout()
        }else{
            startActivity(Intent(activity, LoginActivity::class.java))
        } }
        about_app = view.findViewById(R.id.about_app)
        about_app.setOnClickListener { startActivity(Intent(activity, AboutUsActivity::class.java)) }
        terms = view.findViewById(R.id.terms)
        terms.setOnClickListener { startActivity(Intent(activity, TermsActivity::class.java)) }
        follow_us = view.findViewById(R.id.follow_us)
        follow_us.setOnClickListener { startActivity(Intent(activity, FollowUsActivity::class.java)) }
        whats = view.findViewById(R.id.whats)
        orders = view.findViewById(R.id.orders)
        orders.setOnClickListener {
            if (user.loginStatus!!) {
                startActivity(Intent(activity, OrdersActivity::class.java))
            }else{
                visitor()
            }
        }
        fav = view.findViewById(R.id.fav)
        fav.setOnClickListener {if (user.loginStatus!!) {
            startActivity(Intent(activity, FavouriteActivity::class.java))
        }else{
            visitor()
        }
        }

        getData()
        image = view.findViewById(R.id.image)
        name = view.findViewById(R.id.name)
        email = view.findViewById(R.id.email)
        if (user.loginStatus!!){
            name.visibility = View.VISIBLE
            email.visibility = View.VISIBLE
            image.visibility = View.VISIBLE
            Glide.with(mContext!!).load(user.userData.avatar).into(image)
            name.text = user.userData.name
            email.text = user.userData.email
        }else{
            name.visibility = View.GONE
            email.visibility = View.GONE
            image.visibility = View.GONE
        }
        whats.setOnClickListener { //getLocationWithPermission(phone)
            phone.replaceFirst("0","+966",false)
            openWhatsAppConversationUsingUri(mContext!!,phone,"")
        }
        share_app = view.findViewById(R.id.share_app)
        share_app.setOnClickListener {  CommonUtil.ShareApp(mContext!!)  }
        call_us = view.findViewById(R.id.call_us)
        call_us.setOnClickListener { startActivity(Intent(activity, ContactUsActivity::class.java)) }


    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.WhatsApp()?.enqueue(object :
            Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        phone = response.body()?.data!!
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut("Bearer "+user.userData.token,user.userData.device_id!!,"android",lang.appLanguage)?.enqueue(object :
            Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<TermsResponse>,
                response: Response<TermsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        user.loginStatus=false
                        user.Logout()
                        allCartViewModel!!.deleteItems(cartModelOfflines)
                        CartDataBase.closeDataBase()
                        CommonUtil.makeToast(mContext!!,response.body()?.data!!)
                        startActivity(Intent(activity, SplashActivity::class.java))
                        activity!!.finish()
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)

                    }
                }
            }
        })
    }

    fun openWhatsAppConversationUsingUri(
        context: Context,
        numberWithCountryCode: String,
        message: String
    ) {

        val uri =
            Uri.parse("https://api.whatsapp.com/send?phone=$numberWithCountryCode&text=$message")

        val sendIntent = Intent(Intent.ACTION_VIEW, uri)

        context.startActivity(sendIntent)
    }
    fun visitor(){
        val dialog = Dialog(mContext!!)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog ?.setCancelable(false)
        dialog ?.setContentView(R.layout.dialog_visitor)
        val confirm = dialog?.findViewById<TextView>(R.id.confirm)
        val back = dialog?.findViewById<TextView>(R.id.back)
        confirm?.setOnClickListener {
            dialog?.dismiss()
            startActivity(Intent(activity,LoginActivity::class.java)) }
        back.setOnClickListener { dialog?.dismiss() }
        dialog?.show()
    }
}