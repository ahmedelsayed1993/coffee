package com.aait.coffee.UI.Adapters

import android.content.Context

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.coffee.R
import com.aait.coffee.UI.Fragments.CompletedOrdersFragment

import com.aait.coffee.UI.Fragments.CurrentOrdersFragment


class SubscribeTapAdapter(
    private val context: Context,
    fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            CurrentOrdersFragment()
        }else{
          CompletedOrdersFragment()
        }

    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.current_orders)
        }else{
            context.getString(R.string.completed_orders)
        }
    }
}
