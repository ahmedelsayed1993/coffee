package com.aait.coffee.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.coffee.Base.ParentRecyclerAdapter
import com.aait.coffee.Base.ParentRecyclerViewHolder
import com.aait.coffee.Models.BaristaProductModel
import com.aait.coffee.Models.ProductModel
import com.aait.coffee.R
import com.bumptech.glide.Glide

class ProductsAdapter (context: Context, data: MutableList<ProductModel>, layoutId: Int) :
    ParentRecyclerAdapter<ProductModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.name)
        Glide.with(mcontext).load(questionModel.image).into(viewHolder.image)
        viewHolder.desc.text = questionModel.description
        viewHolder.price.text = questionModel.price+mcontext.getString(R.string.rs)


        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var desc = itemView.findViewById<TextView>(R.id.desc)
        internal var price = itemView.findViewById<TextView>(R.id.price)




    }
}