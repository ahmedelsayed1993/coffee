package com.aait.coffee.UI.Activities

import android.content.Intent
import android.widget.Button
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.R

class BackToMainActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_back_to_main
    lateinit var back: Button

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        back.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","normal")
            startActivity(intent)
            finish()}

    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this,MainActivity::class.java)
        intent.putExtra("type","normal")
        startActivity(intent)
        finish()
    }
}