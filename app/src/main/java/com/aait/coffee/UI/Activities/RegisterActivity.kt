package com.aait.coffee.UI.Activities

import android.content.Intent
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.Models.UserResponse
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.Utils.CommonUtil
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity :Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_register

    lateinit var back:ImageView
    lateinit var name:EditText
    lateinit var phone:EditText
    lateinit var email:EditText
    lateinit var address:TextView
    lateinit var password:EditText
    lateinit var confirm_pass:EditText
    lateinit var sign_up:Button
    lateinit var login:TextView
    var lat = ""
    var lng = ""
    var location = ""

    var deviceID = ""
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        back = findViewById(R.id.back)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        email = findViewById(R.id.email)
        address = findViewById(R.id.address)
        password = findViewById(R.id.password)
        confirm_pass = findViewById(R.id.confirm_pass)
        sign_up = findViewById(R.id.sign_up)
        login = findViewById(R.id.login)
        address.setOnClickListener { startActivityForResult(Intent(this@RegisterActivity,LocationActivity::class.java),1) }
        back.setOnClickListener { onBackPressed()
        finish()
        }
        login.setOnClickListener { startActivity(Intent(this,LoginActivity::class.java)) }
        sign_up.setOnClickListener { if (CommonUtil.checkEditError(name,getString(R.string.name))||
                CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(email,getString(R.string.email))||
                !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
            CommonUtil.checkTextError(address,getString(R.string.address))||
                CommonUtil.checkEditError(password,getString(R.string.password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)||
               CommonUtil.checkEditError(confirm_pass,getString(R.string.confirm_password)) ){
            return@setOnClickListener
        }else {
            if (!password.text.toString().equals(confirm_pass.text.toString())) {
                CommonUtil.makeToast(mContext, getString(R.string.password_not_match))
            }else{
                 Register()
            }
        }
        }


    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data?.getStringExtra("result") != null) {
            location = data?.getStringExtra("result").toString()
            lat = data?.getStringExtra("lat").toString()
            lng = data?.getStringExtra("lng").toString()
            address.text = location
        } else {
            location = ""
            lat = ""
            lng = ""
            address.text = ""
        }

    }

    fun Register(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SignUp(phone.text.toString(),name.text.toString(),email.text.toString()
        ,address.text.toString(),lat,lng,password.text.toString(),deviceID,"android",lang.appLanguage)?.enqueue(object:
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
                Log.e("error", Gson().toJson(t))
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@RegisterActivity,ActivationCodeActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}