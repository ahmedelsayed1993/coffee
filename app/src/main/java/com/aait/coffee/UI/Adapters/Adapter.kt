package com.aait.coffee.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.ImageView
import android.widget.TextView

import com.aait.coffee.Models.ProductModel
import com.aait.coffee.R

import java.util.ArrayList

class Adapter(
    internal var context: Context,
    internal var resource: Int,
    internal var textViewResourceId: Int,
    internal var items: List<ProductModel>
) : ArrayAdapter<ProductModel>(context, resource, textViewResourceId, items) {
    internal var tempItems: List<ProductModel>?=null
    internal var suggestions: MutableList<ProductModel>?=null

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    internal var nameFilter: Filter = object : Filter() {
        override fun convertResultToString(resultValue: Any): CharSequence? {
            return (resultValue as ProductModel).name
        }

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            if (constraint != null) {
                suggestions!!.clear()
                for (people in tempItems!!) {
                    if (people.name!!.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions!!.add(people)
                    }
                }
                val filterResults = Filter.FilterResults()
                filterResults.values = suggestions
                filterResults.count = suggestions!!.size
                return filterResults
            } else {
                return Filter.FilterResults()
            }
        }

        override fun publishResults(constraint: CharSequence, results: Filter.FilterResults) {
            val filterList = results.values as ArrayList<ProductModel>
            if (results != null && results.count > 0) {
                clear()
                for (people in filterList) {
                    add(people)
                    notifyDataSetChanged()
                }
            }
        }
    }

    init {
        tempItems = ArrayList(items) // this makes the difference.
        suggestions = ArrayList()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        if (convertView == null) {
            val inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.recycler_products, parent, false)
        }
        val people = items[position]
        if (people != null) {
            val name = view!!.findViewById<View>(R.id.name) as TextView

                name.text = people.name
        }
        return view!!
    }

    override fun getFilter(): Filter {
        return nameFilter
    }
}