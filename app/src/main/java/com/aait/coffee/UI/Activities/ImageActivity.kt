package com.aait.coffee.UI.Activities

import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.widget.ImageView
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.R
import com.bumptech.glide.Glide
import kotlin.math.max
import kotlin.math.min

class ImageActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_image
    lateinit var back:ImageView
    lateinit var image:ImageView
    var link = ""
    private lateinit var scaleGestureDetector: ScaleGestureDetector
    private var scaleFactor = 1.0f

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
        finish()}
        image = findViewById(R.id.image)
        link = intent.getStringExtra("link")
        Glide.with(mContext).asBitmap().load(link).into(image)
        scaleGestureDetector = ScaleGestureDetector(this, ScaleListener())
    }
    override fun onTouchEvent(motionEvent: MotionEvent): Boolean {
        scaleGestureDetector.onTouchEvent(motionEvent)
        return true
    }


    private inner class ScaleListener : ScaleGestureDetector.SimpleOnScaleGestureListener() {
        override fun onScale(scaleGestureDetector: ScaleGestureDetector): Boolean {
            scaleFactor *= scaleGestureDetector.scaleFactor
            scaleFactor = max(0.1f, min(scaleFactor, 10.0f))
            image.scaleX = scaleFactor
            image.scaleY = scaleFactor
            return true
        }
    }
}