package com.aait.coffee.UI.Activities

import android.content.Intent
import android.os.Build
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.Listeners.OnItemClickListener
import com.aait.coffee.Models.BaristaResponse
import com.aait.coffee.Models.CatModel
import com.aait.coffee.Models.TermsResponse
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.UI.Views.ListDialog
import com.aait.coffee.Utils.CommonUtil
import com.aait.coffee.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class CompaniesActivity :Parent_Activity(),OnItemClickListener{
    override val layoutResource: Int
        get() = R.layout.activity_companies
    lateinit var back:ImageView
    lateinit var name:EditText
    lateinit var phone:EditText
    lateinit var email:EditText
    lateinit var products:TextView
    lateinit var notes:EditText
    lateinit var image:ImageView
    lateinit var confirm:Button
    lateinit var listDialog: ListDialog
    var citiesModels = ArrayList<CatModel>()
   lateinit var cats:CatModel

    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath: String? = null

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        email = findViewById(R.id.email)
        notes = findViewById(R.id.notes)
        image = findViewById(R.id.image)
        products = findViewById(R.id.cats)
        confirm = findViewById(R.id.confirm)
        back.setOnClickListener { onBackPressed()
        finish()}
        products.setOnClickListener { getCities() }
        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }

        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(name,getString(R.string.coffee_name))||
                    CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(email,getString(R.string.email))||
                    !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                    CommonUtil.checkTextError(products,getString(R.string.products))){
                return@setOnClickListener
            }else{
                if (ImageBasePath == null){
                    CommonUtil.makeToast(mContext,getString(R.string.add_image))
                }else{
                    if (notes.text.toString().equals("")){
                        Add(null,ImageBasePath!!)
                    }else{
                        Add(notes.text.toString(),ImageBasePath!!)
                    }
                }
            }
        }

    }

    fun Add(note:String?,path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("image", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.AddCompany(lang.appLanguage,name.text.toString(),phone.text.toString()
        ,email.text.toString(),cats.id!!,note,filePart)?.enqueue(object:Callback<TermsResponse>{
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        val intent = Intent(this@CompaniesActivity,MainActivity::class.java)
                        intent.putExtra("type","normal")
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getCities(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Categories(lang.appLanguage)?.enqueue(object:
            Callback<BaristaResponse> {
            override fun onFailure(call: Call<BaristaResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<BaristaResponse>, response: Response<BaristaResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        citiesModels = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@CompaniesActivity,citiesModels,getString(R.string.products))
                        listDialog.show()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        listDialog.dismiss()
        cats = citiesModels.get(position)
        products.text = cats.name


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]

                Glide.with(mContext).load(ImageBasePath).into(image)

                if (ImageBasePath != null) {

                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }
    }
}