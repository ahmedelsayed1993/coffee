package com.aait.coffee.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.Cart.AllCartViewModel
import com.aait.coffee.Cart.CartDataBase
import com.aait.coffee.Cart.CartModelOffline
import com.aait.coffee.Models.*
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.QuickClient
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.Utils.CommonUtil
import com.google.gson.Gson
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class PayActivity : Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_pay
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var code: EditText
    lateinit var total_text: TextView
    lateinit var discount_text: TextView
    lateinit var online: RadioButton
    lateinit var cash: RadioButton
    lateinit var confirm: Button
    var country = ""
    var lat = ""
    var lng = ""
    var address = ""
    var delivery = ""
    var tax = ""
    var total = ""
    var time = ""
    var date = ""
    var price = 0F
    var tax_value = ""
    var basket = ArrayList<BasketModel>()
    var payment = "cash"
    lateinit var cartOffline: CartModelOffline
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<CartModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<CartModelOffline> = java.util.ArrayList()
    var token = ""
    var city = 0
    override fun initializeComponents() {
        lat = intent.getStringExtra("lat")
        lng = intent.getStringExtra("lng")
        address = intent.getStringExtra("address")
        delivery = intent.getStringExtra("delivery")
        tax = intent.getStringExtra("tax")
        total = intent.getStringExtra("total")
        time = intent.getStringExtra("time")
        date = intent.getStringExtra("date")
        tax_value = intent.getStringExtra("tax_value")
        price = intent.getFloatExtra("price", 0F)
        basket = intent.getSerializableExtra("order") as ArrayList<BasketModel>
        token = intent.getStringExtra("token")
        city = intent.getIntExtra("city", 0)
        country = intent.getStringExtra("country")
        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        allCartViewModel!!.allCart.observe(this, object : Observer<List<CartModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<CartModelOffline>) {
                cartModelOfflines = cartModels
            }
        })
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        code = findViewById(R.id.code)
        total_text = findViewById(R.id.total_text)
        discount_text = findViewById(R.id.discount_text)
        online = findViewById(R.id.online)
        cash = findViewById(R.id.cash)
        confirm = findViewById(R.id.confirm)
        back.setOnClickListener {
            onBackPressed()
            finish()
        }
        title.text = getString(R.string.payment)
        online.setOnClickListener { payment = "online" }
        cash.setOnClickListener { payment = "cash" }
        code.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (event != null && event!!.getKeyCode() === KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (code.text.toString().equals("")) {

                    } else {
                        DoDiscount()
                    }
                }
                return false
            }
        })
        confirm.setOnClickListener {
            if (code.text.toString().equals("")) {
                DoOrder(null, null)
            } else {
                DoOrder(code.text.toString(), null)
            }
        }

    }

    fun DoDiscount() {
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Discount(
            "Bearer " + user.userData.token,
            lang.appLanguage,
            price.toString(),
            code.text.toString(),
            1
        )
            ?.enqueue(object : Callback<discountResponse> {
                override fun onFailure(call: Call<discountResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext, t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<discountResponse>,
                    response: Response<discountResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {
                            total_text.visibility = View.VISIBLE
                            discount_text.visibility = View.VISIBLE
                            CommonUtil.setStrokInText(total_text)
                            total_text.text =
                                response.body()?.total_order.toString() + getString(R.string.rs)
                            tax =
                                ((response.body()?.data!!) * (tax_value.toFloat() / 100)).toString()
                            total =
                                ((response.body()?.data!!) + (response.body()?.data!!) * (tax_value.toFloat() / 100)).toString()
                            Log.e("total", total)
                            discount_text.text =
                                response.body()?.data.toString() + getString(R.string.rs)

                        } else {
                            total_text.visibility = View.GONE
                            discount_text.visibility = View.VISIBLE
                            discount_text.text = response.body()?.msg
                        }
                    }
                }

            })
    }

    fun DoOrder(coupon: String?, confirm: Int?) {
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddOrder("Bearer "+user.userData.token,lang.appLanguage,address,lat
        ,lng,time,date,payment,total,tax,delivery,
            Gson().toJson(basket),coupon,confirm)?.enqueue(object:Callback<OrderResponse>{
            override fun onFailure(call: Call<OrderResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<OrderResponse>, response: Response<OrderResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                       startActivity(Intent(this@PayActivity,BackToMainActivity::class.java))
                        finish()
                        allCartViewModel!!.deleteItems(cartModelOfflines)
                        CartDataBase.closeDataBase()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })


        val body = HashMap<String, Any>()

        var Customer = HashMap<String, Any>()
        Customer.put("Desciption", address)
        Customer.put("Longitude", lng)
        Customer.put("Latitude", lat)
        Customer.put("CountryId", country.toInt())
        Customer.put("CityId", city)




        body.put("CustomerLocation", Customer)
        body.put("SandboxMode", true)
        body.put("CustomerName", user.userData.name!!)
        body.put("CustomerPhoneNumber", user.userData.phone!!)
        body.put(
            "PreferredReceiptTime", date + " " +
                    "" + time
        )
        body.put("PreferredDeliveryTime", date + " " + time)
        body.put("PaymentMethodId", 4)
        body.put("ShipmentContentValueSAR", delivery)
        body.put("ShipmentContentTypeId", 5)


//        QuickClient.getClient()?.create(Service::class.java)?.ShipMent(token,true,user.userData.name!!,user.userData.phone!!
//        ,date+" "+time,date+" "+time,4,delivery,5,
//            emp)?.enqueue(object:Callback<QuickBaseResponse>{


        QuickClient
            .getClient()?.create(Service::class.java)?.ShipMent(token, body)
            ?.enqueue(object : Callback<QuickBaseResponse> {
                override fun onFailure(call: Call<QuickBaseResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext, t)
                    t.printStackTrace()
                    hideProgressDialog()

                }

                override fun onResponse(
                    call: Call<QuickBaseResponse>,
                    response: Response<QuickBaseResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        if (response.body()?.isSuccess!!) {
//                            if (lang.appLanguage.equals("ar")) {
//                                CommonUtil.makeToast(mContext, response.body()?.messageAr!!)
//                            } else {
//                                CommonUtil.makeToast(mContext, response.body()?.messageEn!!)
//                            }
                        } else {
                            if (lang.appLanguage.equals("ar")) {
                                CommonUtil.makeToast(mContext, response.body()?.messageAr!!)
                            } else {
                                CommonUtil.makeToast(mContext, response.body()?.messageEn!!)
                            }
                        }
                    }
                }

            })
    }
}