package com.aait.coffee.UI.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.text.InputType
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.Models.BaseResponse
import com.aait.coffee.Models.UserModel
import com.aait.coffee.Models.UserResponse
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.Utils.CommonUtil
import com.aait.coffee.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ProfileActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_profile
    lateinit var back:ImageView
    lateinit var image:ImageView
    lateinit var phone:EditText
    lateinit var name:EditText
    lateinit var email:EditText
    lateinit var address:TextView
    lateinit var edit:Button
    lateinit var change_pass:Button
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath: String? = null
    var lat = ""
    var lng = ""
    var location = ""

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        image = findViewById(R.id.image)
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        email = findViewById(R.id.email)
        address = findViewById(R.id.address)
        edit = findViewById(R.id.edit)
        change_pass = findViewById(R.id.change_pass)
        back.setOnClickListener { onBackPressed()
        finish()}
        getData()
        address.setOnClickListener { startActivityForResult(Intent(this@ProfileActivity,LocationActivity::class.java),1) }
        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        edit.setOnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                CommonUtil.checkLength1(phone,getString(R.string.phone_length),10)||
                CommonUtil.checkEditError(name,getString(R.string.name))||
                CommonUtil.checkEditError(email,getString(R.string.email))||
                !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                CommonUtil.checkTextError(address,getString(R.string.address))){
                    return@setOnClickListener
                }else{
                update()
            }
        }
        change_pass.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_change_password)
            val old_pass = dialog?.findViewById<EditText>(R.id.old_pass)
            val new_pass = dialog?.findViewById<EditText>(R.id.new_pass)
            val confirm_pass = dialog?.findViewById<EditText>(R.id.confirm_pass)

            val save = dialog?.findViewById<Button>(R.id.confirm)


            save?.setOnClickListener {
                if (CommonUtil.checkEditError(old_pass,getString(R.string.old_password))||
                    CommonUtil.checkEditError(new_pass,getString(R.string.new_password))||
                    CommonUtil.checkLength(new_pass,getString(R.string.password_length),6)||
                    CommonUtil.checkEditError(confirm_pass,getString(R.string.confirm_password))){
                    return@setOnClickListener
                }else{
                    if (!new_pass.text.toString().equals(confirm_pass.text.toString())){
                        confirm_pass.error = getString(R.string.password_not_match)
                    }else{
                        showProgressDialog(getString(R.string.please_wait))
                        Client.getClient()?.create(Service::class.java)?.resetPassword(lang.appLanguage,"Bearer "+user.userData.token,old_pass.text.toString(),new_pass.text.toString())?.enqueue(
                            object :Callback<BaseResponse>{
                                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                                    CommonUtil.handleException(mContext,t)
                                    t.printStackTrace()
                                    hideProgressDialog()
                                }

                                override fun onResponse(
                                    call: Call<BaseResponse>,
                                    response: Response<BaseResponse>
                                ) {
                                    hideProgressDialog()
                                    if (response.isSuccessful){
                                        if (response.body()?.value.equals("1")){
                                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                            dialog?.dismiss()
                                        }else{
                                            CommonUtil.makeToast(mContext,response.body()?.msg!!)

                                        }
                                    }
                                }
                            }
                        )
                    }
                }

            }
            dialog?.show()
        }

    }

    fun SetData(userModel: UserModel){
        Glide.with(mContext).load(userModel.avatar).into(image)
        phone.setText(userModel.phone)
        name.setText(userModel.name)
        email.setText(userModel.email)
        address.text = userModel.address
        lat = userModel.lat!!
        lng = userModel.lng!!
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProfile("Bearer "+user.userData.token,lang.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        SetData(response.body()?.data!!)
                        user.userData = response.body()?.data!!

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode==0){

            }else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]

                Glide.with(mContext).load(ImageBasePath).into(image)

                if (ImageBasePath != null) {
                    upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }else{
            if (data?.getStringExtra("result")!=null) {
                location = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                address.text = location
            }else{
                location = ""
                lat = ""
                lng = ""
                address.text = location
            }
        }
    }

    fun upLoad(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.UpdateImage("Bearer "+user.userData.token,lang.appLanguage,filePart)?.enqueue(object :Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        SetData(response.body()?.data!!)
                        user.userData = response.body()?.data!!
                        CommonUtil.makeToast(mContext,getString(R.string.changed_success))
                        val intent = Intent(this@ProfileActivity,MainActivity::class.java)
                        intent.putExtra("type","normal")
                        startActivity(intent)
                        this@ProfileActivity.finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun update(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.update("Bearer "+user.userData.token,lang.appLanguage,name.text.toString()
            ,address.text.toString(),lat,lng,phone.text.toString(),email.text.toString())?.enqueue(object :Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        SetData(response.body()?.data!!)
                        user.userData = response.body()?.data!!
                        CommonUtil.makeToast(mContext,getString(R.string.changed_success))
                        val intent = Intent(this@ProfileActivity,MainActivity::class.java)
                        intent.putExtra("type","normal")
                        startActivity(intent)
                        this@ProfileActivity.finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}