package com.aait.coffee.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.Models.TermsResponse
import com.aait.coffee.Models.UserModel
import com.aait.coffee.Models.UserResponse
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivationCodeActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_code
     lateinit var back:ImageView
    lateinit var code:EditText
    lateinit var confirm:Button
    lateinit var resend:TextView
    lateinit var userModel: UserModel
    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("user") as UserModel
        back = findViewById(R.id.back)
        code = findViewById(R.id.code)
        confirm = findViewById(R.id.confirm)
        resend = findViewById(R.id.resend)
        back.setOnClickListener { onBackPressed()
        finish()}
        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(code,getString(R.string.enter_code))){
                return@setOnClickListener
            }else{
               Activate()
            }
        }
        resend.setOnClickListener { Resend() }

    }

    fun Activate(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.CheckCode("Bearer "+userModel.token!!,code.text.toString(),lang.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        startActivity(Intent(this@ActivationCodeActivity, LoginActivity::class.java))
                        this@ActivationCodeActivity.finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun Resend(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Resend("Bearer "+userModel.token!!,lang.appLanguage)?.enqueue(object :
            Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<TermsResponse>,
                response: Response<TermsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}