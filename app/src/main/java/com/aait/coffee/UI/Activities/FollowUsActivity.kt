package com.aait.coffee.UI.Activities

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.ImageView
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.Models.SocialResponse
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FollowUsActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_follow_us
    lateinit var face: ImageView
    lateinit var twitter: ImageView
    lateinit var insta: ImageView

    var facebook = ""
    var twit = ""
    var instagram = ""



    override fun initializeComponents() {

        face = findViewById(R.id.facebook)
        twitter = findViewById(R.id.twitter)
        insta = findViewById(R.id.instagram)


        getData()
        face.setOnClickListener {  if (!facebook.equals(""))
        {
            if (facebook.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(facebook)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$facebook"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        }}
        twitter.setOnClickListener {  if (!twit.equals(""))
        {
            if (twit.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(twit)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$twit"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        } }
        insta.setOnClickListener {  if (!instagram.equals(""))
        {
            if (instagram.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(instagram)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$instagram"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        }}

    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.FollowUs()?.enqueue(object :
            Callback<SocialResponse> {
            override fun onFailure(call: Call<SocialResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SocialResponse>,
                response: Response<SocialResponse>
            ) {
                hideProgressDialog()
                if (response.body()?.value.equals("1")){
                    facebook = response.body()?.data?.get(3)?.link!!
                    twit = response.body()?.data?.get(2)?.link!!
                    instagram = response.body()?.data?.get(1)?.link!!

                }else{
                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                }
            }
        })
    }
}