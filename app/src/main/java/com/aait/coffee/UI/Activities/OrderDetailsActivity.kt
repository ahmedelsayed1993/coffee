package com.aait.coffee.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.Models.OrderDetailsModel
import com.aait.coffee.Models.OrderDetailsResponse
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.UI.Adapters.OrderProductAdapter
import com.aait.coffee.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderDetailsActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_order_details
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var products:RecyclerView
    lateinit var follow:Button
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var orderProductAdapter: OrderProductAdapter
    var orderDetailsModels = ArrayList<OrderDetailsModel>()
    var id = 0

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        products = findViewById(R.id.products)
        follow = findViewById(R.id.follow)
        title.text = getString(R.string.order_details)
        back.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","normal")
            startActivity(intent)
        finish()}
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        orderProductAdapter = OrderProductAdapter(mContext,orderDetailsModels,R.layout.recycler_order_product)
        products.layoutManager = linearLayoutManager
        products.adapter = orderProductAdapter
        getData()
        follow.setOnClickListener {
            val intent = Intent(this,FollowOrderActivity::class.java)
            intent.putExtra("id",id)
            startActivity(intent)
        }

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.OrderDetails("Bearer "+user.userData.token,lang.appLanguage,id)
            ?.enqueue(object: Callback<OrderDetailsResponse>{
                override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<OrderDetailsResponse>,
                    response: Response<OrderDetailsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            orderProductAdapter.updateAll(response.body()?.data!!)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}