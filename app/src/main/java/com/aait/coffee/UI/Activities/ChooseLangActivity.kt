package com.aait.coffee.UI.Activities

import android.content.Intent
import android.widget.Button
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.R

class ChooseLangActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_lang
    lateinit var arabic: Button
    lateinit var english:Button
    override fun initializeComponents() {
        arabic = findViewById(R.id.arabic)
        english = findViewById(R.id.english)
        arabic.setOnClickListener {
            lang.appLanguage = "ar"
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","normal")
            startActivity(intent)
            finish()
        }
        english.setOnClickListener {
            lang.appLanguage = "en"
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","normal")
            startActivity(intent)
            finish()
        }


    }
}