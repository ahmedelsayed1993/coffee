package com.aait.coffee.UI.Activities

import android.content.Intent
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.Listeners.OnItemClickListener
import com.aait.coffee.Models.BaristaModel
import com.aait.coffee.Models.BaristaResponse
import com.aait.coffee.Models.CatModel
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.UI.Adapters.BaristaAdapter
import com.aait.coffee.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BaristaActivity:Parent_Activity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this, BaristaProductsActivity::class.java)
        intent.putExtra("id",baristaModels.get(position).id)
        intent.putExtra("image",baristaModels.get(position).image)
        startActivity(intent)

    }

    override val layoutResource: Int
        get() = R.layout.activity_barista
    lateinit var image: ImageView
    lateinit var lay: LinearLayout
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: GridLayoutManager
    var baristaModels=ArrayList<CatModel>()
    lateinit var baristaAdapter: BaristaAdapter
    override fun initializeComponents() {
        image = findViewById(R.id.image)
        lay = findViewById(R.id.lay)
        image.visibility = View.VISIBLE
        lay.visibility = View.GONE
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = GridLayoutManager(mContext, 2)
        baristaAdapter = BaristaAdapter(mContext,baristaModels,R.layout.recycler_barista)
        baristaAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = baristaAdapter
        Handler().postDelayed({
            // logo.startAnimation(logoAnimation2)
            Handler().postDelayed({

                image.visibility = View.GONE
                lay.visibility = View.VISIBLE
            }, 1500)
        }, 1500)

        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()

    }

    fun getData(){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Barista(lang.appLanguage)?.enqueue(object:
            Callback<BaristaResponse> {
            override fun onFailure(call: Call<BaristaResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
//                image.visibility = View.GONE
//                lay.visibility = View.VISIBLE
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<BaristaResponse>,
                response: Response<BaristaResponse>
            ) {
//                image.visibility = View.GONE
//                lay.visibility = View.VISIBLE
                swipeRefresh!!.isRefreshing = false
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        Log.e("myJobs", Gson().toJson(response.body()!!.data))
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            baristaAdapter.updateAll(response.body()!!.data!!)
                        }
//
                    }else {
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }

                }
            }

        })
    }
}