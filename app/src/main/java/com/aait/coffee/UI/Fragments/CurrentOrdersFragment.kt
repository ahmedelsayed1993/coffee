package com.aait.coffee.UI.Fragments

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.coffee.Base.BaseFragment
import com.aait.coffee.Listeners.OnItemClickListener
import com.aait.coffee.Models.BaristaResponse
import com.aait.coffee.Models.CatModel
import com.aait.coffee.Models.OrdersModel
import com.aait.coffee.Models.OrdersResponse
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.UI.Activities.OrderDetailsActivity
import com.aait.coffee.UI.Adapters.BrandsAdapter
import com.aait.coffee.UI.Adapters.OrdersAdapter
import com.aait.coffee.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CurrentOrdersFragment:BaseFragment(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.fragment_orders
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    var baristaModels=ArrayList<OrdersModel>()
    lateinit var ordersAdapter: OrdersAdapter
    override fun initializeComponents(view: View) {
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        ordersAdapter = OrdersAdapter(mContext!!,baristaModels,R.layout.recycler_orders)
        ordersAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = ordersAdapter

        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Orders("Bearer "+user.userData.token,lang.appLanguage,"current")?.enqueue(object:
            Callback<OrdersResponse> {
            override fun onFailure(call: Call<OrdersResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()

                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<OrdersResponse>,
                response: Response<OrdersResponse>
            ) {

                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        Log.e("myJobs", Gson().toJson(response.body()!!.data))
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            ordersAdapter.updateAll(response.body()!!.data!!)
                        }
//
                    }else {
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }

                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(activity, OrderDetailsActivity::class.java)
        intent.putExtra("id",baristaModels.get(position).id)
        startActivity(intent)

    }
}