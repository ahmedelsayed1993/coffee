package com.aait.coffee.UI.Activities

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProviders
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.Cart.AddCartViewModel
import com.aait.coffee.Cart.AllCartViewModel
import com.aait.coffee.Cart.CartDataBase
import com.aait.coffee.Cart.CartModelOffline
import com.aait.coffee.Models.BaristaDetailsResponse
import com.aait.coffee.Models.OrderResponse
import com.aait.coffee.Models.ProductDetailsModel
import com.aait.coffee.Models.ProductDetailsResponse
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.UI.Adapters.SliderAdapter
import com.aait.coffee.Utils.CommonUtil
import com.bumptech.glide.Glide
import com.github.islamkhsh.CardSliderViewPager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductDetailsActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_product_details
    lateinit var back:ImageView
    lateinit var share:ImageView
    lateinit var image:ImageView
    lateinit var fav:ImageView
    lateinit var name:TextView
    lateinit var rates:TextView
    lateinit var rating:RatingBar
    lateinit var description:TextView
    lateinit var plus:ImageView
    lateinit var quantity:TextView
    lateinit var minus:ImageView
    lateinit var price:TextView
    lateinit var add:Button
    var id = 0
    var num = 1
    lateinit var productDetailsModel: ProductDetailsModel
    override lateinit var addCartViewModel: AddCartViewModel
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartDataBase: CartDataBase
    internal lateinit var cartModels: LiveData<List<CartModelOffline>>
    internal var cartModelOfflines: List<CartModelOffline> = java.util.ArrayList()
    lateinit var cartModelOffline: CartModelOffline
    var total = 0F
    var link = ""
    var img = ""

    lateinit var viewpager: CardSliderViewPager
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        viewpager = findViewById(R.id.viewPager)
        back = findViewById(R.id.back)
        share = findViewById(R.id.share)
        image = findViewById(R.id.image)
        fav = findViewById(R.id.fav)
        name = findViewById(R.id.name)
        rates = findViewById(R.id.rates)
        rating = findViewById(R.id.rating)
        description = findViewById(R.id.description)
        plus = findViewById(R.id.plus)
        quantity = findViewById(R.id.quantity)
        minus = findViewById(R.id.minus)
        price = findViewById(R.id.price)
        add = findViewById(R.id.add)
        back.setOnClickListener { onBackPressed()
        finish()}
        cartDataBase = CartDataBase.getDataBase(mContext)
        addCartViewModel = ViewModelProviders.of(this).get(AddCartViewModel::class.java)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        addCartViewModel = AddCartViewModel(application)
        quantity.text = num.toString()
        plus.setOnClickListener {
            num = num+1
            quantity.text = num.toString()
            total = (productDetailsModel.price!!.toFloat()*num)
            price.text = total.toString()+getString(R.string.rs)
        }
        share.setOnClickListener {  CommonUtil.ShareProductName(mContext,link) }
        minus.setOnClickListener {
            if (num >1){
                num = num-1
                Log.e("num",num.toString())
                quantity.text = num.toString()
                total = (productDetailsModel.price!!.toFloat()*num)
                price.text = total.toString()+getString(R.string.rs)
            }

        }
        if (user.loginStatus!!) {
            getData("Bearer "+user.userData.token)
        }else{
            getData(null)
        }
        rates.setOnClickListener { val intent = Intent(this,RatesActivity::class.java)
        intent.putExtra("id",id)
        startActivity(intent)}

        add.setOnClickListener {
            addCartViewModel.addItem(
                CartModelOffline(
                    productDetailsModel.id!!,
                    productDetailsModel.name,
                    productDetailsModel.image,
                    productDetailsModel.description,
                    total.toString(),
                    num
                )
            )
            CommonUtil.makeToast(
                mContext,
                getString(R.string.product_added)
            )
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","cart")
            startActivity(intent)
            finish()
        }

        fav.setOnClickListener {
            if (user.loginStatus!!){
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.Fourite("Bearer "+user.userData.token,lang.appLanguage,id)
                    ?.enqueue(object:Callback<OrderResponse>{
                        override fun onFailure(call: Call<OrderResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                        }

                        override fun onResponse(
                            call: Call<OrderResponse>,
                            response: Response<OrderResponse>
                        ) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    getData("Bearer "+user.userData.token)
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
            }else{
                CommonUtil.makeToast(mContext,getString(R.string.must_login))
            }
        }
        image.setOnClickListener { val intent = Intent(this,ImageActivity::class.java)
            intent.putExtra("link",img)
            startActivity(intent)
        }
    }
    fun getData(token :String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProduct(token,lang.appLanguage,id)?.enqueue(object:
            Callback<ProductDetailsResponse> {
            override fun onFailure(call: Call<ProductDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ProductDetailsResponse>,
                response: Response<ProductDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        productDetailsModel = response.body()?.data!!
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.image).into(image)
                        img = response.body()?.data?.image!!
                        name.text = response.body()?.data?.name
                        description.text = response.body()?.data?.description
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.image)
                        rating.rating = response.body()?.data?.rate!!.toFloat()
                        rates.text = getString(R.string.rates)+"("+response.body()?.data?.comments!!.toInt()+")"
                        price.text = response.body()?.data?.price+getString(R.string.rs)
                        link = response.body()?.data?.share!!
                        initSliderAds(response.body()?.data?.images!!)
                        if (response.body()?.data?.is_favorite==0){
                            fav.setImageResource(R.mipmap.heart_empty)
                        }else{
                            fav.setImageResource(R.mipmap.heart)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun initSliderAds(list:ArrayList<String>){
        if(list.isEmpty()){
            viewpager.visibility= View.GONE
            image.visibility = View.VISIBLE

        }
        else{
            viewpager.visibility= View.VISIBLE
            image.visibility = View.GONE

            viewpager.adapter= SliderAdapter(mContext!!,list)

        }
    }
}