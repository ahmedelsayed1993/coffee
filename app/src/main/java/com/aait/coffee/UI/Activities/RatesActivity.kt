package com.aait.coffee.UI.Activities

import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.Models.*
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.UI.Adapters.CommentsAdapter
import com.aait.coffee.UI.Adapters.ProductsAdapter
import com.aait.coffee.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RatesActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_comments
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var add_comment:Button
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    var baristaModels=ArrayList<CommentsModel>()
    lateinit var baristaAdapter: CommentsAdapter
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
        finish()}
        add_comment = findViewById(R.id.add_comment)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        baristaAdapter = CommentsAdapter(mContext,baristaModels,R.layout.recycler_comments)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = baristaAdapter

        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorWhite
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()

        add_comment.setOnClickListener {
            if (user.loginStatus!!) {
                val dialog = Dialog(mContext)
                dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog?.setCancelable(true)
                dialog?.setContentView(R.layout.dialog_rate)
                val rating = dialog?.findViewById<RatingBar>(R.id.rating)
                val comment = dialog?.findViewById<EditText>(R.id.comment)
                val confirm = dialog?.findViewById<Button>(R.id.confirm)
                confirm!!.setOnClickListener {
                    if (CommonUtil.checkEditError(comment, getString(R.string.write_comment))) {
                        return@setOnClickListener
                    } else {
                        showProgressDialog(getString(R.string.please_wait))
                        Client.getClient()?.create(Service::class.java)?.AddComment(
                            "Bearer " + user.userData.token,
                            lang.appLanguage,
                            id,
                            rating.rating.toInt()
                            ,
                            comment.text.toString()
                        )?.enqueue(object : Callback<TermsResponse> {
                            override fun onFailure(
                                call: Call<TermsResponse>,
                                t: Throwable
                            ) {
                                CommonUtil.handleException(mContext, t)
                                t.printStackTrace()
                                hideProgressDialog()
                            }

                            override fun onResponse(
                                call: Call<TermsResponse>,
                                response: Response<TermsResponse>
                            ) {
                                hideProgressDialog()
                                if (response.isSuccessful) {
                                    if (response.body()?.value.equals("1")) {
                                        CommonUtil.makeToast(mContext, response.body()?.data!!)
                                        dialog?.dismiss()
                                        val intent = Intent(this@RatesActivity,MainActivity::class.java)
                                        intent.putExtra("type","normal")
                                        startActivity(intent)
                                        finish()
                                    } else {
                                        CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                        dialog?.dismiss()
                                        val intent = Intent(this@RatesActivity,MainActivity::class.java)
                                        intent.putExtra("type","normal")
                                        startActivity(intent)
                                        finish()
                                    }
                                }
                            }

                        })
                    }
                }
                dialog?.show()
            }else{
                CommonUtil.makeToast(mContext,getString(R.string.must_login))
            }
        }

    }
    fun getData(){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Comments(lang.appLanguage,id)?.enqueue(object:
            Callback<CommentsResponse> {
            override fun onFailure(call: Call<CommentsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()

                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<CommentsResponse>,
                response: Response<CommentsResponse>
            ) {

                swipeRefresh!!.isRefreshing = false
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        Log.e("myJobs", Gson().toJson(response.body()!!.data))
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            baristaAdapter.updateAll(response.body()!!.data!!)
                        }
//
                    }else {
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }

                }
            }

        })
    }

}