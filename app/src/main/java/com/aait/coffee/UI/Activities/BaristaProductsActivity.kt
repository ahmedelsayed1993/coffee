package com.aait.coffee.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.Listeners.OnItemClickListener
import com.aait.coffee.Models.BaristaDetailsResponse
import com.aait.coffee.Models.BaristaProductModel
import com.aait.coffee.Models.CatModel
import com.aait.coffee.Network.Client
import com.aait.coffee.Network.Service
import com.aait.coffee.R
import com.aait.coffee.UI.Adapters.BaristaAdapter
import com.aait.coffee.UI.Adapters.BaristaProductsAdapter
import com.aait.coffee.Utils.CommonUtil
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BaristaProductsActivity:Parent_Activity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this,ProductDetailsActivity::class.java)
        intent.putExtra("id",baristaModels.get(position).id)

        startActivity(intent)

    }

    override val layoutResource: Int
        get() = R.layout.activity_barista_products
    lateinit var image:ImageView
    lateinit var name:TextView
    lateinit var description:TextView
    lateinit var rv_recycle: RecyclerView

    lateinit var linearLayoutManager: GridLayoutManager
    var baristaModels=ArrayList<BaristaProductModel>()
    lateinit var baristaAdapter: BaristaProductsAdapter
    var img = ""
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        img = intent.getStringExtra("image")
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        description = findViewById(R.id.description)
        rv_recycle = findViewById(R.id.products)
       // Glide.with(mContext).load(img).into(image)
        linearLayoutManager = GridLayoutManager(mContext, 2)
        baristaAdapter = BaristaProductsAdapter(mContext,baristaModels,R.layout.recycler_barista_product)
        baristaAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = baristaAdapter
        getData()
        image.setOnClickListener { val intent = Intent(this,ImageActivity::class.java)
        intent.putExtra("link",img)
            startActivity(intent)
        }


    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.BaristaDetails(lang.appLanguage,id)?.enqueue(object:
            Callback<BaristaDetailsResponse>{
            override fun onFailure(call: Call<BaristaDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<BaristaDetailsResponse>,
                response: Response<BaristaDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.image).into(image)
                        img = response.body()?.data?.image!!
                        name.text = response.body()?.data?.name
                        description.text = response.body()?.data?.description
                        baristaAdapter.updateAll(response.body()?.products!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}