package com.aait.coffee.UI.Activities

import android.view.View
import android.widget.ImageView
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.R
import com.aait.coffee.UI.Adapters.SliderAdapter
import com.github.islamkhsh.CardSliderViewPager

class ImagesActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_images
    lateinit var viewpager: CardSliderViewPager
    var list = ArrayList<String>()
    lateinit var back: ImageView
    override fun initializeComponents() {
        viewpager = findViewById(R.id.viewPager)
        list = intent.getStringArrayListExtra("link") as ArrayList<String>
        initSliderAds(list)
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
            finish()}
    }
    fun initSliderAds(list:ArrayList<String>){
        if(list.isEmpty()){
            viewpager.visibility= View.GONE


        }
        else{
            viewpager.visibility= View.VISIBLE


            viewpager.adapter= SliderAdapter(mContext!!,list)

        }
    }
}