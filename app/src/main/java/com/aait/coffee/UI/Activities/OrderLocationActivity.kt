package com.aait.coffee.UI.Activities

import android.app.TimePickerDialog
import com.aait.coffee.Base.Parent_Activity
import com.aait.coffee.R
import java.util.*
import androidx.databinding.adapters.TextViewBindingAdapter.setText
import android.app.DatePickerDialog
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import com.aait.coffee.GPS.GPSTracker
import com.aait.coffee.GPS.GpsTrakerListener
import com.aait.coffee.Listeners.OnItemClickListener
import com.aait.coffee.Models.BasketModel
import com.aait.coffee.Models.CitiesResponse
import com.aait.coffee.Models.CityModel
import com.aait.coffee.Models.DeliveryResponse
import com.aait.coffee.Network.QuickClient
import com.aait.coffee.Network.Service
import com.aait.coffee.UI.Views.CitiesDialog
import com.aait.coffee.Utils.CommonUtil
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class OrderLocationActivity:Parent_Activity(),OnItemClickListener , OnMapReadyCallback, GoogleMap.OnMapClickListener,
    GpsTrakerListener {
    override fun onMapReady(p0: GoogleMap?) {
        this.googleMap = p0!!
        googleMap.setOnMapClickListener(this)
    }

    override fun onMapClick(p0: LatLng?) {
        Log.e("LatLng", p0.toString())
        lat =  java.lang.Double.toString(p0?.latitude!!)
        lng =  java.lang.Double.toString(p0?.longitude!!)
        if (myMarker != null) {
            myMarker.remove()
            putMapMarker1(p0?.latitude, p0?.longitude)
        } else {
            putMapMarker1(p0?.latitude, p0?.longitude)
        }

        if (p0?.latitude != 0.0 && p0?.longitude != 0.0) {
            putMapMarker1(p0?.latitude, p0?.longitude)
            lat = p0?.latitude.toString()
            lng = p0?.longitude.toString()
            val addresses: List<Address>

            //geocoder = Geocoder(this@OrderLocationActivity, Locale.getDefault())

            try {
                addresses = geocoder.getFromLocation(
                    java.lang.Double.parseDouble(lat),
                    java.lang.Double.parseDouble(lng),
                    1
                )


                if (addresses.isEmpty()) {
                    Toast.makeText(
                        this@OrderLocationActivity,
                        resources.getString(R.string.detect_location),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    address = addresses[0].getAddressLine(0)
                    Log.e("address",address)
                    CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))
                }


            } catch (e: IOException) {
            }

            googleMap.clear()
            putMapMarker1(p0?.latitude, p0?.longitude)
        }
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    fun putMapMarker(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("موقعي")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.location))
        )
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
        // kkjgj

    }
    fun putMapMarker1(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("موقعي")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.location))
        )
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng))
        // kkjgj

    }

    override fun onItemClick(view: View, position: Int) {
        citiesDialog.dismiss()
        cityModel = citiesModels.get(position)
        city = cityModel.id!!
        country = cityModel.countryId!!
        if (lang.appLanguage.equals("ar")){
            location.text = cityModel.name
            address = cityModel.name!!
        }else{
            location.text = cityModel.nameEn
            address = cityModel.nameEn!!
        }
        geocoder = Geocoder(this@OrderLocationActivity, Locale.getDefault())
        val addresses = geocoder.getFromLocationName(location.text.toString(),5)
        val loc = addresses.get(0)
        lat = loc.latitude.toString()
        lng = loc.longitude.toString()
        Log.e("latLng",lat+"::"+lng)
        map.visibility = View.VISIBLE
        putMapMarker(loc.latitude, loc.longitude)

        Delivery(cityModel.id!!)

    }
    internal lateinit var geocoder: Geocoder
var country = ""
    override val layoutResource: Int
        get() = R.layout.activity_order_location
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var location:TextView
    lateinit var time:TextView
    lateinit var date:TextView
    lateinit var payment:Button
    internal lateinit var googleMap: GoogleMap
    internal lateinit var myMarker: Marker
    internal lateinit var map:MapView
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mYear = 0
    var mMonth = 0
    var mDay = 0
    var lat = ""
    var lng = ""
    var address = ""
    var token = ""
    var tax = ""
    var total1 = ""
    var price = 0F
    var tax_value = ""
    var city = 0
    var hour = ""
    var hour1 = ""
    lateinit var lay:LinearLayout
    lateinit var delivery_service:TextView
    var citiesModels = ArrayList<CityModel>()
    lateinit var cityModel: CityModel
    lateinit var citiesDialog: CitiesDialog
    var basket = ArrayList<BasketModel>()
    var delivery = ""
    lateinit var added_tax:TextView
    lateinit var lay1:LinearLayout
    lateinit var lay2:LinearLayout
    lateinit var tax_price:TextView
    lateinit var total:TextView
    @RequiresApi(Build.VERSION_CODES.O)
    override fun initializeComponents() {
        token = intent.getStringExtra("token")
        tax = intent.getStringExtra("tax")
        total1 = intent.getStringExtra("total")
        price = intent.getFloatExtra("price",0F)
        tax_value = intent.getStringExtra("tax_value")
        basket = intent.getSerializableExtra("order") as ArrayList<BasketModel>
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        location = findViewById(R.id.location)
        time = findViewById(R.id.time)
        date = findViewById(R.id.date)
        map = findViewById(R.id.map)
        payment = findViewById(R.id.payment)
        title.text = getString(R.string.detect_location)
        delivery_service = findViewById(R.id.delivery_service)
        lay = findViewById(R.id.lay)
        lay.visibility = View.GONE
        map.visibility = View.GONE
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)

        try {
            MapsInitializer.initialize(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        lay1 = findViewById(R.id.lay1)
        lay2 = findViewById(R.id.lay2)
        total = findViewById(R.id.total)
        added_tax = findViewById(R.id.added_tax)
        tax_price = findViewById(R.id.tax_price)
        back.setOnClickListener { onBackPressed()
        finish()}
        location.setOnClickListener {
            Cities()
        }
        date.setOnClickListener {
            val c = Calendar.getInstance()

            mYear = c.get(Calendar.YEAR)
            mMonth = c.get(Calendar.MONTH)
            mDay = c.get(Calendar.DAY_OF_MONTH)

            // Launch Date Picker Dialog
            val dpd = DatePickerDialog(this,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in textbox
                    date.setText(
                        dayOfMonth.toString() + "/"
                                + (monthOfYear + 1) + "/" + year
                    )
                }, mYear, mMonth, mDay)
            dpd.datePicker.minDate = c.timeInMillis
                dpd.show()  }
        hour=Calendar.getInstance().get(Calendar.HOUR_OF_DAY).toString()
        val current = LocalDateTime.now()

        val formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd")
        val formatter1 = DateTimeFormatter.ofPattern("HH:mm")
        Log.e("date",current.format(formatter).toString())
        Log.e("time",current.format(formatter1).toString())
        time.setOnClickListener {  val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)



            val myTimeListener =
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    if (view.isShown) {
                        myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                        myCalender.set(Calendar.MINUTE, minute)

                        var hour = hourOfDay
                        hour1 = hourOfDay.toString()
                        var am_pm = ""
                        // AM_PM decider logic
                        when {
                            hour == 0 -> {
                                hour += 12
                                am_pm = "AM"
                            }
                            hour == 12 -> am_pm = "PM"
                            hour > 12 -> {
                                hour -= 12
                                am_pm = "PM"
                            }
                            else -> am_pm = "AM"
                        }

                        val hours = if (hour < 10) "0" + hour else hour
                        time.text = " $hours $am_pm"

                    }
                }
            val timePickerDialog = TimePickerDialog(
                this,
                android.R.style.ThemeOverlay_Material_Dialog,
                myTimeListener,
                hour,
                minute,
                false
            )
            timePickerDialog.setTitle(getString(R.string.detect_delivery_time))
            timePickerDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

            timePickerDialog.show() }

        payment.setOnClickListener {
            if (user.loginStatus!!) {
                if (CommonUtil.checkTextError(location, getString(R.string.detect_location))
                ) {
                    return@setOnClickListener
                } else {

                        val intent = Intent(this, PayActivity::class.java)
                        intent.putExtra("delivery", delivery)
                        intent.putExtra("tax", tax)
                        intent.putExtra("total", total.text)
                        intent.putExtra("lat", lat)
                        intent.putExtra("lng", lng)
                        intent.putExtra("token",token)
                        intent.putExtra("price", price)
                        intent.putExtra("address", address)
                        intent.putExtra("city",city)
                        intent.putExtra("country",country)
                        intent.putExtra("tax_value", tax_value)
                        intent.putExtra("time", current.format(formatter1).toString())
                        intent.putExtra("date", current.format(formatter).toString())
                        intent.putExtra("order", basket)
                        startActivity(intent)

                }
            }else{
                CommonUtil.makeToast(mContext,getString(R.string.must_login))
                startActivity(Intent(this,LoginActivity::class.java))
                finish()
            }
        }
    }

    fun Delivery(id:Int){
        showProgressDialog(getString(R.string.please_wait))
        QuickClient.getClient()?.create(Service::class.java)?.Delivery(token,id,4)?.enqueue(object:Callback<DeliveryResponse>{
            override fun onFailure(call: Call<DeliveryResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<DeliveryResponse>,
                response: Response<DeliveryResponse>
            ) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if (response.body()?.httpStatusCode==200){
                        lay.visibility = View.VISIBLE
                        lay1.visibility = View.VISIBLE
                        lay2.visibility = View.VISIBLE
                        delivery_service.text = response.body()?.resultData?.totalCost.toString()
                        delivery = response.body()?.resultData?.totalCost.toString()
                        added_tax.text = getString(R.string.tax)+tax_value + "%"
                        tax_price.text = tax
                        total.text = (total1.toFloat()+response.body()?.resultData?.totalCost!!).toString()

                    }else{
                        if(lang.appLanguage.equals("ar")) {
                            CommonUtil.makeToast(mContext!!, response.body()?.messageAr!!)
                        }else{
                            CommonUtil.makeToast(mContext!!, response.body()?.messageEn!!)
                        }
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data?.getStringExtra("result") != null) {
            address = data?.getStringExtra("result").toString()
            lat = data?.getStringExtra("lat").toString()
            lng = data?.getStringExtra("lng").toString()
            location.text = address
        } else {
            address = ""
            lat = ""
            lng = ""
            location.text = ""
        }

    }

    fun Cities(){
        showProgressDialog(getString(R.string.please_wait))
        QuickClient.getClient()?.create(Service::class.java)?.GetCities(token)?.enqueue(object:
            Callback<CitiesResponse>{
            override fun onFailure(call: Call<CitiesResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<CitiesResponse>,
                response: Response<CitiesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.httpStatusCode==200){
                        citiesModels = response.body()?.resultData?.countryCityList?.get(0)?.cities!!
                        citiesDialog = CitiesDialog(mContext,this@OrderLocationActivity,citiesModels,getString(R.string.city))
                        citiesDialog.show()
                    }else{
                        if(lang.appLanguage.equals("ar")){
                            CommonUtil.makeToast(mContext,response.body()?.messageAr!!)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.messageEn!!)
                        }
                    }
                }
            }

        })
    }
}