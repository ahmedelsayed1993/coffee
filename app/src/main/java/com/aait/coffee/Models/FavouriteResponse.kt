package com.aait.coffee.Models

import java.io.Serializable

class FavouriteResponse:BaseResponse(),Serializable {
    var data:ArrayList<FavouriteModel>?=null
}