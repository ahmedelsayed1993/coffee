package com.aait.coffee.Models

import java.io.Serializable

class OrderDetailsResponse:BaseResponse(),Serializable {
    var data:ArrayList<OrderDetailsModel>?=null
}