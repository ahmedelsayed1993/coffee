package com.aait.coffee.Models

import java.io.Serializable

class NewTokenModel:Serializable {
    var access_token:String?=null
    var token_type:String?=null
}