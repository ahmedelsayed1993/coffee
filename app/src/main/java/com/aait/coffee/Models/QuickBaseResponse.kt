package com.aait.coffee.Models

import java.io.Serializable

open class QuickBaseResponse:Serializable {
    var httpStatusCode:Int?=null
    var isSuccess:Boolean?=null
    var messageAr:String?=null
    var messageEn:String?=null
}