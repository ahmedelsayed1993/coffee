package com.aait.coffee.Models

import java.io.Serializable

class BaristaProductModel:Serializable {
    var id:Int?=null
    var image:String?=null
    var name:String?=null
    var description:String?=null
    var price:String?=null
    var rate:String?=null
}