package com.aait.coffee.Models

import java.io.Serializable

class CitiesModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var alpha2Code:String?=null
    var cities:ArrayList<CityModel>?=null
}