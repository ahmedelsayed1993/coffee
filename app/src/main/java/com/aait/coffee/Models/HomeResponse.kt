package com.aait.coffee.Models

import java.io.Serializable

class HomeResponse:BaseResponse(),Serializable {
    var sliders:ArrayList<String>?=null
    var categories:ArrayList<CatModel>?=null
    var brands:ArrayList<CatModel>?=null
}