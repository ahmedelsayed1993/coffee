package com.aait.coffee.Models

import java.io.Serializable

class BaristaDetailsResponse :BaseResponse(),Serializable {
    var data:BaristaModel?=null
    var products:ArrayList<BaristaProductModel>?=null
}