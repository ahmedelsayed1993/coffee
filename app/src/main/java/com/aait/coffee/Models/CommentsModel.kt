package com.aait.coffee.Models

import java.io.Serializable

class CommentsModel:Serializable {
    var id:Int?=null
    var avatar:String?=null
    var name:String?=null
    var comment:String?=null
    var rate:String?=null
}