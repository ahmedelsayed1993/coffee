package com.aait.coffee.Models

import java.io.Serializable

class OrdersModel:Serializable {
    var id:Int?=null
    var date:String?=null
    var payment:String?=null
}