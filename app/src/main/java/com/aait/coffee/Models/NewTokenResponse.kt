package com.aait.coffee.Models

import java.io.Serializable

class NewTokenResponse:QuickBaseResponse(),Serializable {
    var resultData:NewTokenModel?=null
}