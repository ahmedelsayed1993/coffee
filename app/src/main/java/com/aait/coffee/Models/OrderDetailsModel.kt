package com.aait.coffee.Models

import java.io.Serializable

class OrderDetailsModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var category:String?=null
    var description:String?=null
    var price:String?=null
    var image:String?=null
}