package com.aait.coffee.Models

import java.io.Serializable

class CityModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var nameEn:String?=null
    var countryId:String?=null
}