package com.aait.coffee.Models

import java.io.Serializable

class CustomerLocation:Serializable {
    var Desciption:String?=null
    var Longitude:String?=null
    var Latitude:String?=null
    var GoogleMapsFullLink:String?=null
    var CountryId:Int?=null
    var CityId:Int?=null

    constructor(
        Desciption: String?,
        Longitude: String?,
        Latitude: String?,
        CountryId: Int?,
        CityId: Int?
    ) {
        this.Desciption = Desciption
        this.Longitude = Longitude
        this.Latitude = Latitude

        this.CountryId = CountryId
        this.CityId = CityId
    }
}