package com.aait.coffee.Models

import java.io.Serializable

class ProductModel:Serializable {
    var id:Int?=null
    var image:String?=null
    var name:String?=null
    var description:String?=null
    var price:String?=null
}