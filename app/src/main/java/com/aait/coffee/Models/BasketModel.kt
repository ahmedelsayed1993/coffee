package com.aait.coffee.Models

import java.io.Serializable

class BasketModel:Serializable {
    var product_id:String?=null
    var count:Int?=null
    var price:String?=null

    constructor(product_id: String?, count: Int?,price:String?) {
        this.product_id = product_id
        this.count = count
        this.price = price
    }
}