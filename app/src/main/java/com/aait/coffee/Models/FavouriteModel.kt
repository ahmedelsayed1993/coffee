package com.aait.coffee.Models

import java.io.Serializable

class FavouriteModel:Serializable {
    var id:Int?=null
    var product_id:Int?=null
    var image:String?=null
    var name:String?=null
    var rate:Float?=null
    var price:String?=null
}