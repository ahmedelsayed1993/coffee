package com.aait.coffee.Models

import java.io.Serializable

class DeliveryModel:Serializable {
    var totalCost:Float?=null
    var totalCostCurrency:String?=null
}