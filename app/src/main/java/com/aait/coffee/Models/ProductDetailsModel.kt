package com.aait.coffee.Models

import java.io.Serializable

class ProductDetailsModel :Serializable{
    var image:String?=null
    var rate:String?=null
    var is_favorite:Int?=null
    var comments:Int?=null
    var name:String?=null
    var description:String?=null
    var price:String?=null
    var id:Int?=null
    var share:String?=null
    var images:ArrayList<String>?=null
}