package com.aait.coffee.Network

import com.aait.coffee.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*
import java.util.HashMap

interface Service {

    @POST("home")
    fun getHome(@Query("lang") lang:String):Call<HomeResponse>

    @POST("sign-up")
    fun SignUp(@Query("phone") phone:String,
               @Query("name") name:String,
               @Query("email") email:String,
               @Query("address") address:String,
               @Query("lat") lat:String,
               @Query("lng") lng:String,
               @Query("password") password:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Query("lang") lang: String):Call<UserResponse>

    @POST("check-code")
    fun CheckCode(@Header("Authorization") Authorization:String,
                  @Query("code") code:String,
                  @Query("lang") lang: String):Call<UserResponse>

    @POST("resend-code")
    fun Resend(@Header("Authorization") Authorization:String,
               @Query("lang") lang: String):Call<TermsResponse>

    @POST("sign-in")
    fun Login(@Query("phone") phone:String,
              @Query("password") password:String,
              @Query("device_id") device_id:String,
              @Query("device_type") device_type:String,
              @Query("lang") lang: String):Call<UserResponse>

    @POST("baristas")
    fun Barista(@Query("lang") lang:String):Call<BaristaResponse>

    @POST("brands")
    fun Brands(@Query("lang") lang: String):Call<BaristaResponse>

    @POST("categories")
    fun Categories(@Query("lang") lang: String):Call<BaristaResponse>

    @POST("get-products-barista")
    fun BaristaDetails(@Query("lang") lang: String,
                       @Query("barista_id") barista_id:Int):Call<BaristaDetailsResponse>

    @POST("details-product")
    fun getProduct(@Header("Authorization") Authorization:String?,
                   @Query("lang") lang: String,
                   @Query("product_id") product_id:Int):Call<ProductDetailsResponse>

    @POST("get-products")
    fun Products(@Query("lang") lang: String,
                 @Query("type_id") type_id:Int,
                 @Query("type") type:String):Call<ProductsResponse>

    @POST("filter")
    fun Filter(@Query("lang") lang: String,
               @Query("type_id") type_id: Int,
               @Query("type") type: String,
               @Query("price") price:String):Call<ProductsResponse>

    @POST("terms")
    fun Terms(@Query("lang") lang: String):Call<TermsResponse>
    @POST("about")
    fun About(@Query("lang") lang: String):Call<TermsResponse>
    @POST("follow-us")
    fun FollowUs():Call<SocialResponse>
    @POST("contact-whatsapp")
    fun WhatsApp():Call<TermsResponse>
    @POST("notifications")
    fun Notifications(@Header("Authorization") Authorization:String,
                      @Query("lang") lang: String):Call<NotificationResponse>
    @POST("delete-notification")
    fun delete(
        @Header("Authorization") Authorization:String,
        @Query("notification_id") notification_id:Int,
        @Query("lang") lang: String):Call<TermsResponse>

    @POST("contact-us")
    fun Contact(@Query("lang") lang:String,
                @Query("name") name:String?,
                @Query("phone") phone:String?,
                @Query("message") message:String?):Call<SocialResponse>

    @POST("product-comments")
    fun Comments(@Query("lang") lang:String,
                 @Query("product_id") product_id:Int):Call<CommentsResponse>
    @POST("costs")
    fun Coast():Call<CoastResponse>

    @POST("similar-products")
    fun Similar(@Query("lang") lang: String,
                @Query("product_id") product_id:Int):Call<SimilarProductsResponse>

    @POST("discount")
    fun Discount(@Header("Authorization") Authorization:String,
                 @Query("lang") lang: String,
                 @Query("total") total:String,
                 @Query("coupon") coupon:String,
                 @Query("confirm") confirm:Int):Call<discountResponse>

    @POST("add-order")
    fun AddOrder(@Header("Authorization") Authorization:String,
                 @Query("lang") lang: String,
                 @Query("address") address: String,
                 @Query("lat") lat: String,
                 @Query("lng") lng: String,
                 @Query("time") time:String,
                 @Query("date") date:String,
                 @Query("payment") payment:String,
                 @Query("total") total:String,
                 @Query("tax") tax:String,
                 @Query("delivery_price") delivery_price:String,
                 @Query("carts") carts:String,
                 @Query("coupon") coupon: String?,
                 @Query("confirm") confirm: Int?):Call<OrderResponse>

    @POST("user-orders")
    fun Orders(@Header("Authorization") Authorization:String,
               @Query("lang") lang: String,
               @Query("status") status:String):Call<OrdersResponse>

    @POST("details-order")
    fun OrderDetails(@Header("Authorization") Authorization:String,
                     @Query("lang") lang: String,
                     @Query("order_id") order_id:Int):Call<OrderDetailsResponse>

    @POST("track-order")
    fun Track(@Header("Authorization") Authorization:String,
              @Query("lang") lang: String,
              @Query("order_id") order_id:Int):Call<TermsResponse>
    @POST("track-order")
    fun Finish(@Header("Authorization") Authorization:String,
              @Query("lang") lang: String,
              @Query("order_id") order_id:Int,
               @Query("status") status:String,
               @Query("confirm") confirm:Int):Call<TermsResponse>

    @POST("evaluations")
    fun RateOrder(@Header("Authorization") Authorization:String,
                  @Query("lang") lang: String,
                  @Query("order_id") order_id:Int,
                  @Query("rate") rate:Int,
                  @Query("comment") comment:String):Call<TermsResponse>

    @POST("add-comment")
    fun AddComment(@Header("Authorization") Authorization:String,
                   @Query("lang") lang: String,
                   @Query("product_id") product_id:Int,
                   @Query("rate") rate:Int,
                   @Query("comment") comment:String):Call<TermsResponse>

    @POST("favorite")
    fun Fourite(@Header("Authorization") Authorization:String,
                @Query("lang") lang: String,
                @Query("product_id") product_id:Int):Call<OrderResponse>

    @POST("user-favorites")
    fun Favourites(@Header("Authorization") Authorization:String,
                   @Query("lang") lang: String):Call<FavouriteResponse>

    @POST("edit-profile")
    fun getProfile(@Header("Authorization") Authorization:String,
                   @Query("lang") lang: String):Call<UserResponse>
    @Multipart
    @POST("edit-profile")
    fun UpdateImage(@Header("Authorization") Authorization:String,
                    @Query("lang") lang: String,
                    @Part avater: MultipartBody.Part):Call<UserResponse>

    @POST("edit-profile")
    fun update(@Header("Authorization") Authorization:String,
               @Query("lang") lang: String,
               @Query("name") name:String,
               @Query("address") address:String,
               @Query("lat") lat:String,
               @Query("lng") lng: String,
               @Query("phone") phone:String,
               @Query("email") email: String):Call<UserResponse>
    @POST("reset-password")
    fun resetPassword(@Query("lang") lang:String,
                      @Header("Authorization") Authorization:String,
                      @Query("current_password") current_password:String,
                      @Query("password") password:String):Call<BaseResponse>

    @Multipart
    @POST("companies-registration")
    fun AddCompany(@Query("lang") lang:String,
                   @Query("name") name:String,
                   @Query("phone") phone: String,
                   @Query("email") email:String,
                   @Query("category_id") category_id:Int,
                   @Query("notes") notes:String?,
                   @Part image:MultipartBody.Part):Call<TermsResponse>
    @POST("forget-password")
    fun ForGot(@Query("phone") phone:String,
               @Query("lang") lang:String):Call<UserResponse>

    @POST("update-password")
    fun NewPass(@Header("Authorization") Authorization:String,
                @Query("password") password:String,
                @Query("code") code:String,
                @Query("lang") lang: String):Call<UserResponse>
    @POST("log-out")
    fun logOut(@Header("Authorization") Authorization:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Query("lang") lang:String):Call<TermsResponse>

    @POST("access-token")
    fun NewToken():Call<TermsResponse>

//    @Headers("Content-Type: application/json")
    @FormUrlEncoded
    @POST("Login/GetAccessToken")
    fun Token(
        @Field("UserName") UserName:String,
        @Field("Password") Password:String):Call<NewTokenResponse>

    @FormUrlEncoded
    @POST("V3/Store/Shipment/GetShippingCost")
    fun Delivery(@Header("Authorization") Authorization:String,
                 @Field("CityId") CityId:Int,
                 @Field("PaymentMethodId") PaymentMethodId:Int):Call<DeliveryResponse>

//    @Headers( "Content-Type: application/json" )
//    @FormUrlEncoded
//    @POST("V3/Store/Shipment")
//    fun ShipMent(
//        @Header("Authorization") Authorization:String,
//                 @Field("SandboxMode") SandboxMode:Boolean,
//                 @Field("CustomerName") CustomerName:String,
//                 @Field("CustomerPhoneNumber") CustomerPhoneNumber:String,
//                 @Field("PreferredReceiptTime") PreferredReceiptTime:String,
//                 @Field("PreferredDeliveryTime") PreferredDeliveryTime:String,
//                 @Field("PaymentMethodId") PaymentMethodId:Int,
//                 @Field("ShipmentContentValueSAR") ShipmentContentValueSAR:String,
//                 @Field("ShipmentContentTypeId") ShipmentContentTypeId:Int,
//                 @Field("CustomerLocation") CustomerLocation: JSONObject
//    ):Call<QuickBaseResponse>
  @Headers( "Content-Type: application/json" )
    @POST("V3/Store/Shipment")
    fun ShipMent(
    @Header("Authorization") Authorization:String,
    @Body body: HashMap<String, Any>

):Call<QuickBaseResponse>

    @GET("V3/GetConsistentData")
    fun GetCities(@Header("Authorization") Authorization:String):Call<CitiesResponse>
}