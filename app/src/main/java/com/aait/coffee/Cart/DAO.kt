package com.aait.coffee.Cart


import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

import androidx.room.OnConflictStrategy.REPLACE


@Dao
interface DAO {
    @get:Query("select * from CartModelOffline")
    val allCartItems: LiveData<List<CartModelOffline>>


    @Insert(onConflict = REPLACE)
    fun addItem(cartModelOffline: CartModelOffline): Long?
    //
    //    @Insert(onConflict = REPLACE)
    //    void addOrder(OrderModel orderModel);

    @Delete
    fun deleteItem(cartModel:  CartModelOffline)

    //    @Delete
    //    void deleteOrder(OrderModel orderModel);

    @Delete
    fun deleteItems(cartModel: List<CartModelOffline>)

    //    @Delete
    //    void deleteOrders(List<OrderModel> orderModels);

    @Update
    fun updateItem(cartModel: CartModelOffline)


    @Query("select * from CartModelOffline where product_id =:productId")
    fun selectItem(productId: Int): LiveData<CartModelOffline>



}
